import { dataElementosComunes } from '../createPanelCharts';
import { isMobile } from './logic-helpers';

export function drawChartElements(div, chart){
    let commonElement = dataElementosComunes.filter((item) => {if(item.ID_Grafico.trim() == chart){return item;}});
    commonElement = commonElement[0];
    
    let mobile = isMobile();
    
    /* Título */
    let contenedorTitulo = document.createElement('div');
    contenedorTitulo.setAttribute('class', 'widget__title-container');
    let titular = document.createElement('h3');
    titular.setAttribute('class', mobile ? 'widget__title widget__title--panel' : 'widget__title widget__title--panel hidden__mobile');
    let titulo = document.createElement('span');
    if(commonElement.Area_Interes_Desarrollo == 'termometro'){
        titulo.textContent = 'Recuperación. ' + commonElement.Titulo;
    } else {
        titulo.textContent = commonElement.Titulo;
    }
    

    /* Número de gráfico */
    /*let numeroGrafico = document.createElement('span');
    numeroGrafico.setAttribute('class', 'widget__title-position');
    numeroGrafico.textContent = div.getAttribute('data-position');

    titular.appendChild(numeroGrafico);*/
    titular.appendChild(titulo);
    

    //Incrustamos titular en nuevo bloque
    contenedorTitulo.appendChild(titular);

    //Incrustamos nuevo bloque en bloque global
    div.appendChild(contenedorTitulo);

    /* Caja de gráfico */
    if(mobile){
        let graficoContenedor = document.createElement('div');
        graficoContenedor.setAttribute('class', 'panel-chart graph--chart');

        div.appendChild(graficoContenedor);
    } else {
        let contenedorSuperior = document.createElement('div');
        contenedorSuperior.setAttribute('class', 'hidden-mobile');
        let graficoContenedor = document.createElement('div');
        graficoContenedor.setAttribute('class', 'chart__container');
        let grafico = document.createElement('div');
        grafico.setAttribute('class', 'panel-chart chart')
        
        graficoContenedor.appendChild(grafico);
        contenedorSuperior.appendChild(graficoContenedor);
        div.appendChild(contenedorSuperior);
    }
    

    /* Fuente de datos */
    let fuenteContenedor = document.createElement('div');
    fuenteContenedor.setAttribute('class', 'row row--end row--acenter');
    let fuente = document.createElement('span');
    fuente.setAttribute('class', 'source source--panel');
    fuente.textContent = 'Fuente: ' + commonElement.Fuente;
    fuenteContenedor.appendChild(fuente);
    div.appendChild(fuenteContenedor);
}