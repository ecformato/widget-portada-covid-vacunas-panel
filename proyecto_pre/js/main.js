import '@babel/polyfill';
import { getURLParam, drawCovidFirst, drawPanelFirst } from './common_utilities/dom-logic-elements';
import { createCovidCharts } from './Covid/createCovidCharts';
import { createPanelCharts } from './Panel/createPanelCharts';
import { drawTooltip, getOutTooltips } from './common_utilities/tooltip';
//Necesario para importar los estilos de forma automática en la etiqueta 'style' del html final
import '../css/main.scss';

let widgetElement = document.getElementById('widgetElement');
let tabCovid = document.getElementById('tabCovid');
let tabPanel = document.getElementById('tabPanel');
let modulesContainer = document.getElementById('modulesContainer');
let columnsContainerCovid = document.getElementById('columnsContainerCovid');
let columnsContainerPanel = document.getElementById('columnsContainerPanel');

window.addEventListener('DOMContentLoaded', function() {
    let modulo = getURLParam();
    modulo == 'covid' ? createCovidCharts() : createPanelCharts();
    modulo == 'covid' ? drawCovidFirst() : drawPanelFirst();

    //En cualquier caso, creamos tooltip y también incorporamos la opción de borrado de tooltip (cuando los mouseleave de d3 no funcionen correctamente)
    drawTooltip();
    let chartBlocks = document.getElementsByClassName('column');
    for(let i = 0; i < chartBlocks.length; i++){
        chartBlocks[i].addEventListener('mouseleave', function(){
            getOutTooltips();
        })
    }
});

tabCovid.addEventListener('click', function(e) {
    e.preventDefault();
    
    if(!this.classList.contains('tab--active')){
        //Actualizamos fondo del widget
        widgetElement.classList.remove('widget--panel');
        widgetElement.classList.add('widget--covid');
        //Actualizamos tab activo
        tabPanel.classList.remove('tab--active');
        this.classList.add('tab--active');
        //Actualizamos módulo activo
        if (this.classList.contains('tab--first')) {
            modulesContainer.classList.remove('modules--translated');
        } else {
            modulesContainer.classList.add('modules--translated');
        }
    }
    
    let elementoCovid = document.getElementById('vaccine-histogram');
    if(!elementoCovid.hasChildNodes()){
        createCovidCharts();
    }

    getOutTooltips();
});

tabPanel.addEventListener('click', function(e) {
    e.preventDefault();
    
    if(!this.classList.contains('tab--active')){
        //Actualizamos fondo del widget
        widgetElement.classList.remove('widget--covid');
        widgetElement.classList.add('widget--panel');
        //Actualizamos tab activo
        tabCovid.classList.remove('tab--active');
        this.classList.add('tab--active');
        //Actualizamos módulo activo
        if (this.classList.contains('tab--first')) {
            modulesContainer.classList.remove('modules--translated');
        } else {
            modulesContainer.classList.add('modules--translated');
        }        
    }

    let elementoPanel = document.getElementById('panel-chart-1');
    if(!elementoPanel.hasChildNodes()){
        createPanelCharts();
    }
    
    getOutTooltips();
});

/* 
* 
* Mobile Navigation (para Covid) 
*
*/
let leftMobileArrowCovid = document.getElementById('left-mobile-arrow--covid');
let rightMobileArrowCovid = document.getElementById('right-mobile-arrow--covid');
let currentCovidChartMobile = 'vaccine_evolucion';

leftMobileArrowCovid.addEventListener('click', toggleCovidGraphMobile, false);
rightMobileArrowCovid.addEventListener('click', toggleCovidGraphMobile, false);

function toggleCovidGraphMobile(e){
    //console.log(e.target.id);
    if(e.target.id == 'left-mobile-arrow--covid' || e.target.id == 'right-mobile-arrow--covid'){
        if(e.target.id == 'left-mobile-arrow--covid'){
            if(currentCovidChartMobile == 'vaccine_evolucion'){
                //Visualización del gráfico
                columnsContainerCovid.style.transform = 'translateX(-'+columnsContainerCovid.offsetWidth+'px)';
                //Modificación de la variable genérica
                currentCovidChartMobile = 'evolucion_casos';
            } else {
                //Visualización del gráfico
                columnsContainerCovid.style.transform = 'translateX(0)';
                //Modificación de la variable genérica
                currentCovidChartMobile = 'vaccine_evolucion';
                //Actualizamos flecha disabled
                leftMobileArrowCovid.classList.add('mobile__arrow--disabled');
                rightMobileArrowCovid.classList.remove('mobile__arrow--disabled');
            }

        } else {
            if(currentCovidChartMobile == 'vaccine_evolucion'){
                //Visualización del gráfico
                columnsContainerCovid.style.transform = 'translateX(-'+columnsContainerCovid.offsetWidth+'px)';
                //Modificación de la variable genérica
                currentCovidChartMobile = 'evolucion_casos';
                //Actualizamos flecha disabled
                leftMobileArrowCovid.classList.remove('mobile__arrow--disabled');
                rightMobileArrowCovid.classList.add('mobile__arrow--disabled');
            } else {
                //Visualización del gráfico
                columnsContainerCovid.style.transform = 'translateX(0)';
                //Modificación de la variable genérica
                currentCovidChartMobile = 'vaccine_evolucion';
            }
        }
    }
    getOutTooltips();
}

/* 
* 
* Mobile Navigation (para Panel) 
*
*/
let leftMobileArrowPanel = document.getElementById('left-mobile-arrow--panel');
let rightMobileArrowPanel = document.getElementById('right-mobile-arrow--panel');
let currentPanelChartMobile = 'panel-chart-1';

leftMobileArrowPanel.addEventListener('click', togglePanelGraphMobile, false);
rightMobileArrowPanel.addEventListener('click', togglePanelGraphMobile, false);

function togglePanelGraphMobile(e) {
    if(e.target.id == 'left-mobile-arrow--panel' || e.target.id == 'right-mobile-arrow--panel'){
        if(e.target.id == 'left-mobile-arrow--panel'){
            if(currentPanelChartMobile == 'panel-chart-1'){
                //Visualización del gráfico
                columnsContainerPanel.style.transform = 'translateX(-'+columnsContainerPanel.offsetWidth+'px)';
                //Modificación de la variable genérica
                currentPanelChartMobile = 'panel-chart-2';
            } else {
                //Visualización del gráfico
                columnsContainerPanel.style.transform = 'translateX(0)';
                //Modificación de la variable genérica
                currentPanelChartMobile = 'panel-chart-1';
                //Actualizamos flecha disabled
                leftMobileArrowPanel.classList.add('mobile__arrow--disabled');
                rightMobileArrowPanel.classList.remove('mobile__arrow--disabled');
            }

        } else {
            if(currentPanelChartMobile == 'panel-chart-1'){
                //Visualización del gráfico
                columnsContainerPanel.style.transform = 'translateX(-'+columnsContainerPanel.offsetWidth+'px)';
                //Modificación de la variable genérica
                currentPanelChartMobile = 'panel-chart-2';
                //Actualizamos flecha disabled
                leftMobileArrowPanel.classList.remove('mobile__arrow--disabled');
                rightMobileArrowPanel.classList.add('mobile__arrow--disabled');
            } else {
                //Visualización del gráfico
                columnsContainerPanel.style.transform = 'translateX(0)';
                //Modificación de la variable genérica
                currentPanelChartMobile = 'panel-chart-1';
            }
        }
    }
}