import { json } from 'd3-request';
import { select, event } from 'd3-selection';
import { scaleBand, scaleTime, scaleLinear } from 'd3-scale';
import { max, min, extent, range } from 'd3-array';
import { axisBottom, axisLeft, axisRight } from 'd3-axis';
import { line as d3line, area as d3area } from 'd3-shape';

import { format } from 'd3-format';
import { feature } from 'topojson-client';
import { geoPath } from 'd3-geo';
import { geoConicConformalSpain } from 'd3-composite-projections';

import { formatTime, formatTimeV2, formatDayTime } from '../Helpers/time-helpers'; //formatTimeData
import { meses, getNumberWithCommas, calculateRegression } from '../Helpers/logic-helpers';
import { drawChartElements } from '../Helpers/draw-helpers';
import { tooltipDiv, formatTooltipDate } from '../../common_utilities/tooltip';

const mapaAutonomias = 'https://www.ecestaticos.com/file/2d25f5ae64e34b03073ee5c2654863c0/1595321804-autonomias_ceuta_melilla_mapshaper.json';

//Producción
function getProduccionPibHistograma(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    let lineaEjeX = proyecto == 'dashboard' ? !slideWidth ? 'area-crecimiento-trim-ejeX' : 'slider-crecimiento-trim-ejeX' : 'crecimiento-trim-ejeX';
    
    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartProduccionPibHistograma(data, contenedorGrafico, lineaEjeX, proyecto) : 
            getChartProduccionPibHistograma(data, contenedorGrafico, lineaEjeX, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart1-1');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartProduccionPibHistograma(data, grafico, lineaEjeX, proyecto);
    }

    function getChartProduccionPibHistograma(data, grafico, lineaEjeX, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Estructura fundamental del gráfico
        let margin = {top: 5, right: 12.5, bottom: 17.75, left: 27.5},
        width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
        height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom;
        
        //Inicialización del gráfico
        let chart = select(grafico)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr('transform', 'translate(' + margin.left + ',' + margin.top +')');

        //Eje X
        let x = scaleBand()
            .range([0,width])
            .domain(range(data.length));

        let x2 = scaleTime()
            .range([0,width])
            .domain(extent(data, function(d) { return formatTime(d.trimestre_v2) }));

        let xAxis = function(svg){
            svg.call(axisBottom(x2).tickFormat(function(d){ return d.getFullYear(); }))
            svg.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {if(d.getFullYear() == 1996 || d.getFullYear() == 2004 || d.getFullYear() == 2012 ||d.getFullYear() == 2020){return 1} else {return 0}})})
            svg.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d) {return d.getFullYear() == 2020 ? 'end' : 'middle' })})
            svg.call(function(g){g.selectAll('line').remove()})
            svg.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','0')});
        }        

        chart.append('g')
            .attr('transform', 'translate(0, ' + height + ')')
            .call(xAxis);
        
        //Eje Y
        let y0 = Math.max(Math.abs(min(data, function(d) {return d.pib_data})), Math.abs(max(data, function(d) {return d.pib_data})));

        let y = scaleLinear()
            .domain([-y0, Math.ceil(max(data, function(d){ return +d.pib_data }))])
            .range([height,0])
            .nice();

        let yAxis = function(svg){
            svg.call(axisLeft(y).ticks(6))
            svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) { return d == 0 && 'tick-text-special'})})
            svg.call(function(g){g.select('.domain').remove()})
            svg.call(function(g){g.selectAll('.tick line')
                .attr('class', function(d) {return d == 0 && 'zero-base' })
                .attr("x1", '0%')
                .attr("x2", '' + (document.getElementById('' + lineaEjeX + '').getBoundingClientRect().width) + '')});
        }        

        chart.append('g')
            .attr("transform", 'translate(0,0)')
            .call(yAxis);

        chart.selectAll('.barras')
            .data(data)
            .enter()
            .append('rect')
            .attr('class', function(d) {return d.pib_data < 0 ? 'fill--below-zero' : 'fill--above-zero'})
            .attr('y', function(d) { return y(Math.max(0,d.pib_data)) })
            .attr('x', function(d,i) { return x(i)})
            .attr('height', function(d) {return Math.abs(y(d.pib_data) - y(0));})
            .attr('width', x.bandwidth() / 2)
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 300;">' + d.trimestre + '</span><span style="font-weight: 900;">' + d.pib_data.toFixed(2).replace('.',',') + '%</span>')
                    .style("left", ""+ (event.pageX - 25) +"px")     
                    .style("top", ""+ (event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDiv.style('display','none').style('opacity','0');
            })
    }    
}

function getProduccionInversionProductiva(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){ //Gráfico de fiebre
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    
    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartProduccionInversionProductiva(data, contenedorGrafico, proyecto) : 
            getChartProduccionInversionProductiva(data, contenedorGrafico, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart1-2');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartProduccionInversionProductiva(data, grafico, proyecto);
    }

    function getChartProduccionInversionProductiva(data, grafico, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Estructura fundamental del gráfico
        let margin = {top: 2.5, right: 12.5, bottom: 17.75, left: 27.5},
        width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
        height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom;
        
        //Inicialización del gráfico
        let chart = select(grafico)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr('transform', 'translate('+ margin.left + ',' + margin.top + ')');

        //Eje X
        let x = scaleBand()
            .domain(data.map(function(d) { return d.trimestre}))
            .range([0, width])
            .padding(0.1);
        
        let xAxis = function(svg){
            svg.call(axisBottom(x).tickFormat(function(d) { return d.substr(-4)}).tickValues(x.domain().filter(function(d,i) { return !(i%32) })))
            svg.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d) {return d.substr(-4) == 1995 ? 'start' : d.substr(-4) == 2019 ? 'end' : 'middle'})})
            svg.call(function(g){g.selectAll('.tick line').remove()});
        }        
        
        chart.append('g')
            .attr('transform', 'translate(0, ' + height +')')
            .call(xAxis);

        //Eje Y
        let y = scaleLinear()
            .domain(extent(data, function(d) {return d.ip_data}))
            .range([height,0]);

        let yAxis = function(svg){
            svg.attr("transform", 'translate(0,0)')
            svg.call(axisLeft(y).ticks(7))
            svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) { return d == 0 && 'tick-text-special'})})
            svg.call(function(g){g.selectAll('.tick line').remove()});
        }        
        
        chart.append("g")
            .attr('transform', 'translate(0, 0)')
            .call(yAxis);

        //Line
        let line = d3line()
            .defined(function(d) { return !isNaN(d.ip_data)})
            .x(function(d) { return x(d.trimestre)})
            .y(function(d) { return y(d.ip_data)});
        
        chart.append("path")
            .datum(data.filter(line.defined()))
            .attr('class', 'area-lines')
            .attr('transform', 'translate(0, 0)')
            .attr("d", line);

        //Area > set the gradient
        chart.append("linearGradient")				
        .attr("id", ((proyecto == 'dashboard' && slideWidth) || proyecto != 'dashboard') ? 'slider-gradient' : 'area-gradient')			
        .attr("gradientUnits", "userSpaceOnUse")	
        .attr("x1", 0).attr("y1", y(min(data, function(d) {return d.ip_data})))			
        .attr("x2", 0).attr("y2", y(max(data, function(d) {return d.ip_data})))		
        .selectAll("stop")						
        .data([
            {offset: "0%", color: "rgba(255, 70, 70, 0)"},
            {offset: "80%", color: "rgb(255, 70, 70, 0.4)"},
            {offset: "100%", color: "rgba(255, 70, 70, 1)"}	
        ])					
        .enter()
        .append("stop")			
        .attr("offset", function(d) { return d.offset; })	
        .attr("stop-color", function(d) { return d.color; });

        let area = d3area()
            .x(function(d) { return x(d.trimestre); })	
            .y0(height)					
            .y1(function(d) { return y(d.ip_data); });

        chart.append('path')
            .datum(data)
            .attr('fill', ((proyecto == 'dashboard' && slideWidth) || proyecto != 'dashboard') ? 'url(#slider-gradient)' : 'url(#area-gradient)')
            .attr('d', area);

        //Rectángulo para tooltip
        chart.selectAll('.circles')
            .data(data)
            .enter()
            .append('circle')
            .attr("r", 5)
            .attr("cx", function(d) { return x(d.trimestre); })
            .attr("cy", function(d) { return y(d.ip_data); })
            .style("fill", '#000')
            .style('opacity', '0')
            .attr('transform', 'translate(2.5, 0)')
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 300;">' + d.trimestre + '</span><span style="font-weight: 900;">' + d.ip_data.toFixed(2).replace('.',',') + '%</span>')
                    .style("left", ""+ (event.pageX - 25) +"px")     
                    .style("top", ""+ (event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDiv.style('display','none').style('opacity','0');
            })
    }    
}

function getProduccionPibEurozona(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){ //Barras Eurozona
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    let lineaEjeY = proyecto == 'dashboard' ? !slideWidth ? 'area-produccion-pib-ue-ejeY' : 'slider-produccion-pib-ue-ejeY' : 'produccion-pib-ue-ejeY';
    
    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartProduccionPibEurozona(data, contenedorGrafico, lineaEjeY, proyecto) : 
            getChartProduccionPibEurozona(data, contenedorGrafico, lineaEjeY, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart1-3');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartProduccionPibEurozona(data, grafico, lineaEjeY, proyecto);
    }

    function getChartProduccionPibEurozona(data, grafico, lineaEjeY, proyecto, slideWidth = undefined, slideHeight = undefined) {
        //Primero, desarrollo y dibujo de los botones
        let menosAltura = 0;

        //Estructura de datos
        let newData = [];

        for(let i = 0; i < data.length; i++){
            if(data[i].pais == 'UE' || data[i].pais == 'Eurozona' || data[i].pais == 'Alemania' || data[i].pais == 'Grecia' || data[i].pais == 'España' || data[i].pais == 'Francia' ||
            data[i].pais == 'Italia' || data[i].pais == 'P. Bajos' || data[i].pais == 'Portugal' || data[i].pais == 'Reino Unido' || data[i].pais == 'Austria'){
                newData.push(data[i]);
            }
        }

        function comparativa(a,b){
            const datoA = isNaN(a.dato_trimestre_2) ? 0 : a.dato_trimestre_2;
            const datoB = isNaN(b.dato_trimestre_2) ? 0 : b.dato_trimestre_2;
    
            let comparacion = 0;
            if (datoA > datoB) {
                comparacion = -1;
            } else if (datoA <= datoB) {
                comparacion = 1;
            }
            return comparacion;
        }
    
        newData = newData.sort(comparativa);
        
        //Estructura fundamental del gráfico
        let margin = {top: 5, right: 15, bottom: 17.75, left: 80},
        width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
        height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom - menosAltura;

        let chart = select(grafico)
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        // Y axis
        let y = scaleBand()
        .range([0, height])
        .domain(newData.map(function(d) { return d.pais; }));

        let yAxis = function(g){
            g.call(axisLeft(y))
            g.call(function(g){g.selectAll('.domain').attr('id', lineaEjeY).style('opacity','0')})
            g.call(function(g){g.selectAll('.tick text').style('font-size', '13px').style('opacity','1').style('font-weight','300')})
            g.call(function(g){g.selectAll('.tick line').remove()});
        }        

        chart.append("g")
        .call(yAxis);

        //X axis
        let maxData = Math.ceil(max(newData, function(d){ return +d.dato_trimestre_2 })) < 0 ? 0 : Math.ceil(max(newData, function(d){ return +d.dato_trimestre_2 }));

        let x = scaleLinear()
        .range([0, width])
        .domain([Math.floor(min(newData, function(d){ return +d.dato_trimestre_2 })), maxData]);
        
        let xAxis = function(g){
            g.call(axisBottom(x).ticks(5))
            g.call(function(g){g.selectAll('.domain').remove()})
            g.call(function(g){g.selectAll('.tick text').attr('class', function(d){return d == 0 && 'tick-text-special'})})
            g.call(function(g){g.selectAll('.tick line')
                .attr('class', function(d) {return d == 0 && 'zero-base' })
                .attr("y1", '0%')
                .attr("y2", '-' + (document.getElementById(lineaEjeY).getBoundingClientRect().height) + '')})
            g.attr("transform", "translate(0," + (height - 5) + ")");
        }
        
        chart.append("g")
            .attr('class','x-axis')
            .call(xAxis);

        function initChart(){
            // Lines
            chart.selectAll("myline")
                .data(newData)
                .enter()
                .append("line")
                .attr("x1", function(d) { return x(0); })
                .attr("x2", function(d) { return !isNaN(d.dato_trimestre_2) ? x(d.dato_trimestre_2) : x(0); })
                .attr("y1", function(d) { return y(d.pais) + (y.bandwidth() / 2); })
                .attr("y2", function(d) { return y(d.pais) + (y.bandwidth() / 2); })
                .attr('class', 'mylines stroke--specific')
                .on('touchstart mouseover', function(d){
                    let dato = d.dato_trimestre_2;
            
                    tooltipDiv.transition()     
                        .duration(200)
                        .style('display', 'block')
                        .style('opacity','0.9')
                    tooltipDiv.html('<span style="font-weight: 900;">' + dato.toFixed(2).replace('.',',') + '%</span>')
                        .style("left", ""+ (event.pageX - 25) +"px")     
                        .style("top", ""+ (event.pageY - 35) +"px");
                })
                .on('touchend mouseout mouseleave', function(d) {
                    tooltipDiv.style('display','none').style('opacity','0');
                })
        }

        initChart();
    }    
}

function getProduccionProdIndustrial(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){ //Mapa por CCAA
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    
    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getMapProduccionProdIndustrial(data, contenedorGrafico, proyecto) : 
            getMapProduccionProdIndustrial(data, contenedorGrafico, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart1-5');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getMapProduccionProdIndustrial(data, grafico, proyecto);
    }

    function getMapProduccionProdIndustrial(data, grafico, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Creación de leyenda   
        let divLeyenda = document.createElement('div');
        divLeyenda.setAttribute('class','legend');

        let primerPunto = document.createElement('p');
        primerPunto.setAttribute('class','legend__item legend__item--primero');
        primerPunto.textContent = 'Menos de -10';

        let segundoPunto = document.createElement('p');
        segundoPunto.setAttribute('class','legend__item legend__item--segundo');
        segundoPunto.textContent = '-10 a -5';

        let tercerPunto = document.createElement('p');
        tercerPunto.setAttribute('class','legend__item legend__item--tercero');
        tercerPunto.textContent = '-5 a 0';

        let cuartoPunto = document.createElement('p');
        cuartoPunto.setAttribute('class','legend__item legend__item--cuarto');
        cuartoPunto.textContent = 'Más de 0';

        divLeyenda.appendChild(primerPunto);
        divLeyenda.appendChild(segundoPunto);
        divLeyenda.appendChild(tercerPunto);
        divLeyenda.appendChild(cuartoPunto);

        json(mapaAutonomias, function(error, mapa){
            if(error) throw error;
            //No hay datos para Ceuta y Melilla, por lo que no es necesario recuperarlas
            let coordenadasMapa = feature(mapa, mapa.objects.autonomous_regions);
            let datosMapa = coordenadasMapa.features;

            data.forEach(function(item) {
                datosMapa.forEach(function(item2) { //Sólo quedan sin datos Ceuta y Melilla
                    if(item.id_ccaa == parseInt(item2.id)){
                        item2.ccaa = item.ccaa;
                        item2.pi_data = item.pi_data;
                    }
                })
            });    
            
            //Estructura fundamental del gráfico
            let width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth),
            height = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - 25;
                
            //Inicialización del gráfico
            let chart = select(grafico)
                .append('svg')
                .attr('width', width)
                .attr('height', height);

            const projection = geoConicConformalSpain();
            const path = geoPath(projection.fitSize([width,height], coordenadasMapa));

            chart.selectAll(".region")
                .data(datosMapa)
                .enter()
                .append("path")
                .attr('id', function(d) {return d.ccaa})
                .attr("d", path)
                .attr('class', function(d) {return d.pi_data < -10 ? 'stroke-in-maps map-fill--worst' : d.pi_data < -5 && d.pi_data >= -10 ? 'stroke-in-maps map-fill--worse' : d.pi_data < 0 && d.pi_data >= -5 ? 'stroke-in-maps map-fill--better' : 'stroke-in-maps map-fill--best'})
                .on('touchstart mouseover', function(d) {
                    tooltipDiv.transition()     
                        .duration(200)
                        .style('display', 'block')
                        .style('opacity','0.9')
                    tooltipDiv.html('<span style="font-weight: 300;">' + d.ccaa + '</span><span style="font-weight: 900;">' + d.pi_data.toFixed(2).replace('.',',') + '%</span>')
                        .style("left", ""+ (event.pageX - 25) +"px")     
                        .style("top", ""+ (event.pageY - 35) +"px");
                })
                .on('touchend mouseout mouseleave', function(d) {
                    tooltipDiv.style('display','none').style('opacity','0');
                })

                grafico.appendChild(divLeyenda);
        });

        
    }    
}

function getProduccionPibEmpleo(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){ //Gráfico de dispersión    
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    let lineaEjeX = proyecto == 'dashboard' ? !slideWidth ? 'area-pibempleoEjeX' : 'slider-pibempleoEjeX' : 'pibempleoEjeX';

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartProduccionPibEmpleo(data, contenedorGrafico, lineaEjeX, proyecto) : 
            getChartProduccionPibEmpleo(data, contenedorGrafico, lineaEjeX, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart1-6');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartProduccionPibEmpleo(data, grafico, lineaEjeX, proyecto);
    }

    function getChartProduccionPibEmpleo(data, grafico, lineaEjeX, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Estructura fundamental del gráfico
        let margin = {top: 20, right: 10, bottom: 17.75, left: 27.5},
        width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
        height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom;
        
        //Inicialización del gráfico
        let chart = select(grafico)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');
        
        //Eje x
        let x = scaleLinear()
            .domain([Math.floor(min(data, function(d) { return d.horas_efectivas_data - 1})),Math.ceil(max(data, function(d) { return d.horas_efectivas_data + 1}))])
            .range([0, width]);
        
        let xAxis = function(svg){
            svg.call(axisBottom(x).ticks(5))
            svg.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','0')})
            svg.call(function(g){g.selectAll('.tick line')
                .attr('class', function(d) {return d == 0 && 'zero-base' })
                .attr("y1", '0%')
                .attr("y2", ((proyecto == 'dashboard' && slideWidth) || proyecto != 'dashboard') ? '-94.15%' : '-87%')})
            svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})});
        }        

        chart.append('g')
            .attr('transform', 'translate(0,' + height + ')')        
            .call(xAxis);
        
        //Eje y
        let y = scaleLinear()
            .domain([Math.floor(min(data, (d) => { return d.pib_correlacion_data})),Math.ceil(max(data, (d) => { return d.pib_correlacion_data}))])
            .range([height, 0]);

        let yAxis = function(svg){
            svg.call(axisLeft(y).ticks(5))
            svg.attr('id', ((proyecto == 'dashboard' && slideWidth) || proyecto != 'dashboard') ? 'slider-pibempleoEjeY' : 'area-pibempleoEjeY')
            svg.call(function(g){g.select('.domain').remove()})
            svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
            svg.call(function(g){g.selectAll('.tick line')
                .attr('class', function(d) {return d == 0 && 'zero-base' }) //No tenía el d == 0 y el stroke no era #343434 sino #151515 (pero lo dejamos así de momento)
                .attr("x1", '0%')
                .attr("x2", '' + (document.getElementById(lineaEjeX).getBoundingClientRect().width) + '')});
        }        

        chart.append('g')
            .attr('transform', 'translate(0,0)')
            .call(yAxis)

        //Línea de regresión
        let datosX = data.map(function(d) { return d.horas_efectivas_data; });
        let datosY = data.map(function(d) { return d.pib_correlacion_data; });
        let regresion = calculateRegression(datosX, datosY);

        let line = d3line()
            .x(function(d) {
                return x(d.horas_efectivas_data);
            })
            .y(function(d) {
                return y(regresion(d.horas_efectivas_data));
            });

        chart.append("path")
            .datum(data)
            .attr('class', 'regression-line')
            .attr("d", line);

        //Dibujo del gráfico
        chart.append('g')
            .selectAll("dot")
            .data(data)
            .enter()
            .append('circle')
            .attr('class', 'dispersion-circles')
            .attr('r', ((proyecto == 'dashboard' && slideWidth) || proyecto != 'dashboard') ? 4.25 : 3.5)
            .attr('cx', function(d) { return x(d.horas_efectivas_data) })
            .attr('cy', function(d) { return y(d.pib_correlacion_data) })
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 300;">Trimestre:</span> <span style="font-weight: 900;">' + d.trimestre + '</span><span style="font-weight: 300;">PIB:</span><span style="font-weight: 900;">' + d.pib_correlacion_data.toFixed(2).replace('.',',') + '</span><span style="font-weight: 300;">Ocupados:</span><span style="font-weight: 900;">' + d.horas_efectivas_data.toFixed(2).replace('.',',') + '</span>')
                    .style("left", ""+ (event.pageX - 40) +"px")     
                    .style("top", ""+ (event.pageY - 50) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDiv.style('display','none').style('opacity','0');
            });   
    }
}

//Expectativas
function getExpectativasPrevisionEconomica(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartExpectativasPrevisionEconomica(data, contenedorGrafico, proyecto) : 
            getChartExpectativasPrevisionEconomica(data, contenedorGrafico, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart2-1');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartExpectativasPrevisionEconomica(data, grafico, proyecto);
    }

    function getChartExpectativasPrevisionEconomica(data, grafico, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Estructura fundamental del gráfico
        let margin = {top: 2.5, right: 7.5, bottom: 17.75, left: 27.5},
        width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
        height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom;
        
        //Inicialización del gráfico
        let chart = select(grafico)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

        //Eje X
        let x = scaleBand()
            .range([0,width])
            .domain(range(data.length));

        let x2 = scaleBand()
            .range([0,width])
            .domain(data.map((function(d) { return d.mes })));

        let xAxis = function(svg){
            svg.call(axisBottom(x2).tickFormat(function(d,i) {if(i == 0){return '2007'} else if(i == data.length -1){ return '2020'}}))
            svg.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d,i) {return i == 0 ? 'start' : i == data.length -1 ? 'end' : 'middle' })})
            svg.call(function(g){g.selectAll('.tick line').remove()});
        }        

        chart.append('g')
            .attr('transform', 'translate(0, ' + height + ')')
            .call(xAxis);
        
        //Eje Y
        let y = scaleLinear()
            .domain([Math.floor(min(data, function(d) {return d.situacion_economica})), Math.ceil(max(data, function(d) {return d.situacion_economica}))])
            .range([height,0])
            .nice();

        let yAxis = function(svg){
            svg.call(axisLeft(y).ticks(6))
            svg.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
            svg.call(function(g){g.select('.domain').remove()});
        }        

        chart.append('g')
            .call(yAxis);

        //Histograma
        chart.selectAll('.barras')
            .data(data)
            .enter()
            .append('rect')
            .attr('class', function(d) {return d.situacion_economica < 0 ? 'fill--below-zero' : 'fill--above-zero'})
            .attr('y', function(d) { return y(Math.max(0,d.situacion_economica)) })
            .attr('x', function(d,i) { return x(i)})
            .attr('height', function(d) {return Math.abs(y(d.situacion_economica) - y(0));})
            .attr('width', x.bandwidth() / 2)
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><span style="font-weight: 900;">' + d.situacion_economica.toFixed(2).replace('.',',') + '%</span>')
                    .style("left", ""+ (event.pageX - 25) +"px")     
                    .style("top", ""+ (event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDiv.style('display','none').style('opacity','0');
            })
    }
}

function getExpectativasPrevisionParo(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartExpectativasPrevisionParo(data, contenedorGrafico, proyecto) : 
            getChartExpectativasPrevisionParo(data, contenedorGrafico, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart2-2');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartExpectativasPrevisionParo(data, grafico, proyecto);
    }

    function getChartExpectativasPrevisionParo(data, grafico, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Estructura fundamental del gráfico
        let margin = {top: 13.5, right: 7.5, bottom: 17.75, left: 27.5},
        width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
        height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom;
        
        //Inicialización del gráfico
        let chart = select(grafico)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //Eje X
        let x = scaleBand()
            .range([0,width])
            .domain(range(data.length));

        let x2 = scaleBand()
            .range([0,width])
            .domain(data.map(function(d) { return d.mes }))

        let xAxis = function(g){
            g.call(axisBottom(x2).tickFormat(function(d,i) {if(i == 0){return '2007'} else if(i == data.length -1){ return '2020'}}))
            g.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d,i) {return i == 0 ? 'start' : i == data.length -1 ? 'end' : 'middle' })})
            g.call(function(g){g.selectAll('.tick line').remove()});
        }        

        chart.append('g')
            .attr('transform', 'translate(0, ' + height + ')')
            .call(xAxis);
        
        //Eje Y
        let y = scaleLinear()
            .domain([Math.floor(min(data, function(d) {return d.situacion_empleo})), Math.ceil(max(data, function(d) {return d.situacion_empleo}))])
            .range([height,0])
            .nice();

        let yAxis = function(g){
            g.call(axisLeft(y).ticks(6))
            g.call(function(g){g.selectAll('.tick text').attr('class', function(d){return d == 0 && 'tick-text-special'})})
            g.call(function(g){g.select('.domain').remove()});
        }        

        chart.append('g')
            .call(yAxis);

        chart.selectAll('.barras')
            .data(data)
            .enter()
            .append('rect')
            .attr('class', function(d) {return d.situacion_empleo < 0 ? 'fill--below-zero' : 'fill--above-zero'})
            .attr('y', function(d) { return y(Math.max(0,d.situacion_empleo)) })
            .attr('x', function(d,i) { return x(i)})
            .attr('height', function(d) {return Math.abs(y(d.situacion_empleo) - y(0));})
            .attr('width', x.bandwidth() / 2)
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><span style="font-weight: 900;">' + d.situacion_empleo.toFixed(2).replace('.',',') + '%</span>')
                    .style("left", ""+ (event.pageX - 25) +"px")     
                    .style("top", ""+ (event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d){
                tooltipDiv.style('display','none').style('opacity','0');
            })
    }
}

function getExpectativasPrevisiones(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){ //Barras agrupadas
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    let lineaEjeY = proyecto == 'dashboard' ? !slideWidth ? 'area-expectativas-previsiones-ejeY' : 'slider-expectativas-previsiones-ejeY' : 'expectativas-previsiones-ejeY';

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartExpectativasPrevisiones(data, contenedorGrafico, lineaEjeY, proyecto) : 
            getChartExpectativasPrevisiones(data, contenedorGrafico, lineaEjeY, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart2-3');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartExpectativasPrevisiones(data, grafico, lineaEjeY, proyecto);
    }

    function getChartExpectativasPrevisiones(data, grafico, lineaEjeY, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Creación de leyenda         
        let divLeyenda = document.createElement('div');
        divLeyenda.setAttribute('class','legend-three');

        let primerPunto = document.createElement('p');
        primerPunto.setAttribute('class','legend__item legend__item--primero');
        primerPunto.textContent = 'III trim.';

        let segundoPunto = document.createElement('p');
        segundoPunto.setAttribute('class','legend__item legend__item--segundo');
        segundoPunto.textContent = 'IV trim.';

        let tercerPunto = document.createElement('p');
        tercerPunto.setAttribute('class','legend__item legend__item--tercero');
        tercerPunto.textContent = 'I trim.';

        divLeyenda.appendChild(primerPunto);
        divLeyenda.appendChild(segundoPunto);
        divLeyenda.appendChild(tercerPunto);

        //Estructura fundamental del gráfico
        let margin = {top: 10, right: 80, bottom: 17.75, left: 17.5},
        width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
        height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom - 25;
        
        //Modificación del formato de datos
        let newData = [];
        for(let i = 0; i < data.length; i++){
            if(data[i].pais == 'P. Bajos' || data[i].pais == 'Austria' || data[i].pais == 'Grecia' || data[i].pais == 'Alemania' || data[i].pais == 'Eurozona' ||
                data[i].pais == 'Portugal' || data[i].pais == 'Italia' || data[i].pais == 'España' || data[i].pais == 'Francia'){
                    newData.push({categoria: data[i]['pais'], valores: [{descriptor:'I trim.', valor: data[i].tercer_trim}, 
                        {descriptor:'IV trim.', valor: data[i].segundo_trim}, {descriptor:'III trim.', valor: data[i].primer_trim} ]});
            }           
        }

        let paises = newData.map(function(d) { return d.categoria });
        let descriptores = newData[0].valores.map(function(d) { return d.descriptor});

        //Estructura del gráfico
        let chart = select(grafico)
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //Eje Y
        let y0 = scaleBand()
            .rangeRound([height,0])
            .domain(paises)
            .padding(.5)
            .paddingOuter(.1);

        let y1 = scaleBand()
            .range([y0.bandwidth(),0])
            .domain(descriptores);

        let yAxis = function(g){
            g.call(axisRight(y0).tickSize(0))
            g.call(function(g){g.selectAll('.tick text')
                .style('font-size', '13px')
                .style('font-weight',function(d){if(d == 'España'){return '500'} else {return '300'}})
                .style('opacity','1')
            })
            g.call(function(g){g.select('.domain').attr('id', lineaEjeY).style('opacity','0')});
        }        

        chart.append("g")        
            .attr('transform', 'translate(' + (width + 5) + ',0)')
            .call(yAxis);
        
        //Eje X
        let minData = -12, maxData = 0;
        for(let i = 0; i < data.length; i++){
            let datos = Object.values(data[i]);
            for(let z = 0; z < datos.length; z++){
                if(!isNaN(datos[z])){
                    if(datos[z] > maxData){
                        maxData = datos[z];
                    }
                    if(datos[z] < minData){
                        minData = datos[z];
                    }
                }
            }
        }

        let x = scaleLinear()
            .domain([Math.ceil(maxData), Math.floor(minData)])
            .range([width,0]);

        let xAxis = function(g){
            g.call(axisBottom(x).ticks(5))
            g.call(function(g){g.select('.domain').remove()})
            g.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
            g.call(function(g){g.selectAll('.tick line')
                .attr('class', function(d) {return d == 0 && 'zero-base' })
                .attr("y1", '0%')
                .attr("y2", '-' + (document.getElementById(lineaEjeY).getBoundingClientRect().height) + '')});
        }        

        chart.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);
        
        //Visualización de datos
        let slice = chart.selectAll(".slice")
            .data(newData)
            .enter()
            .append("g")
            .attr("class", "g")
            .attr("transform",function(d) { return "translate(0," + y0(d.categoria) + ")"; });

        slice.selectAll("rect")
            .data(function(d) { return d.valores; })
            .enter()
            .append("rect")
            .attr("width", function(d) { return Math.abs(x(d.valor) - x(0)); })
            .attr('height', y1.bandwidth())
            .attr('class',  function(d){return d.descriptor == 'III trim.' ? 'fill--primero' : d.descriptor == 'IV trim.' ? 'fill--segundo' : 'fill--tercero'})
            .attr('x', function(d) { return d.valor > 0 ? x(0) : x(d.valor)})
            .attr('y', function(d) { return y1(d.descriptor)})
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 900;">' + d.valor.toFixed(2).replace('.',',') + '%</span>')
                    .style("left", ""+ (event.pageX - 25) +"px")     
                    .style("top", ""+ (event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDiv.style('display','none').style('opacity','0');
            })

        grafico.appendChild(divLeyenda);
    }
}

function getExpectativasConfianzaConsumidor(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartExpectativasConfianzaConsumidor(data, contenedorGrafico, proyecto) : 
            getChartExpectativasConfianzaConsumidor(data, contenedorGrafico, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart2-4');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartExpectativasConfianzaConsumidor(data, grafico, proyecto);
    }

    function getChartExpectativasConfianzaConsumidor(data, grafico, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Creación de leyenda          
        let divLeyenda = document.createElement('div');
        divLeyenda.setAttribute('class','legend-three');

        let primerPunto = document.createElement('p');
        primerPunto.setAttribute('class','legend__item legend__item--primero');
        primerPunto.textContent = 'Manufacturas';

        let segundoPunto = document.createElement('p');
        segundoPunto.setAttribute('class','legend__item legend__item--tercero');
        segundoPunto.textContent = 'Servicios';

        let tercerPunto = document.createElement('p');
        tercerPunto.setAttribute('class','legend__item legend__item--cuarto');
        tercerPunto.textContent = 'Total';

        divLeyenda.appendChild(primerPunto);
        divLeyenda.appendChild(segundoPunto);
        divLeyenda.appendChild(tercerPunto);

        //Estructura fundamental del gráfico
        let margin = {top: 10, right: 5, bottom: 17.75, left: 27.5},
        width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
        height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom - 25;

        //Inicialización del gráfico
        let chart = select(grafico)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        
        let x = scaleBand()
            .domain(data.map(function(d) { return d.mes_v2 }))
            .range([0, width])
            .padding(0.1);
        
        let maxData = 60;
        for(let i = 0; i < data.length; i++){
            let datos = Object.values(data[i]);
            for(let z = 0; z < datos.length; z++){
                if(!isNaN(datos[z])){
                    if(datos[z] > maxData){
                        maxData = datos[z];
                    }
                }
            }
        }
        
        let y = scaleLinear()
            .range([height,0])
            .domain([0, maxData]);

        //Eje X
        let xAxis = function(g){
            g.call(axisBottom(x).tickFormat(function(d) {return d.substr(-4)}))
            g.call(function(g){g.selectAll('.tick line').remove()})
            g.call(function(g){g.selectAll('.tick').attr('opacity', function(d,i) {return i == 0 || i == data.length - 1 ? '1' : '0'}).attr('text-anchor',function(d,i) {return i == 0 ? 'start' : i == data.length -1 ? 'end' : 'middle'})});
        }
            
        chart.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

        //Eje Y
        let yAxis = function(g){
            g.call(axisLeft(y).ticks(5))
            g.call(function(g){g.selectAll('.tick line').remove()})
            g.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
        }        
        
        chart.append("g")
            .call(yAxis);

        // define the 1st line
        var valueline = d3line()
            .x(function(d) { return x((d.mes_v2)); })
            .y(function(d) { return y(d.manufacturas); });

        // define the 2nd line
        var valueline2 = d3line()
            .x(function(d) { return x(d.mes_v2); })
            .y(function(d) { return y(d.servicios); });

        var valueline3 = d3line()
            .x(function(d) { return x(d.mes_v2); })
            .y(function(d) { return y(d.total_economia); });

        // Add the valueline path.
        chart.append("path")
            .data([data])
            .attr('class', 'stroke--primero')
            .attr("d", valueline);

        // Add the valueline2 path.
        chart.append("path")
            .data([data])
            .attr('class', 'stroke--segundo')
            .attr("d", valueline2);

        // Add the valueline2 path.
        chart.append("path")
            .data([data])
            .attr('class', 'stroke--tercero')
            .attr("d", valueline3);

        //Añadir círculos para las tres líneas
        //Manufacturas
        chart.selectAll('.circles')
            .data(data)
            .enter()
            .append('circle')
            .attr("r", 5)
            .attr("cx", function(d) { return x(d.mes_v2); })
            .attr("cy", function(d) { return y(d.manufacturas); })
            .style("fill", '#000')
            .style('opacity', '0')
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('background-color', '#661e1e')
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><span style="font-weight: 900;">' + d.manufacturas.toFixed(1).replace('.',',') + '%</span>')
                    .style("left", ""+ (event.pageX - 25) +"px")     
                    .style("top", ""+ (event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDiv.style('display','none').style('opacity','0').style('background-color', '#f7f7f7');
            })

        //Servicios
        chart.selectAll('.circles')
            .data(data)
            .enter()
            .append('circle')
            .attr("r", 5)
            .attr("cx", function(d) { return x(d.mes_v2); })
            .attr("cy", function(d) { return y(d.servicios); })
            .style("fill", '#000')
            .style('opacity', '0')
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('background-color', '#ff4a4a')
                    .style('display', 'block')
                    .style('opacity','0.9')
                    tooltipDiv.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><span style="font-weight: 900;">' + d.servicios.toFixed(1).replace('.',',') + '%</span>')
                    .style("left", ""+ (event.pageX - 25) +"px")     
                    .style("top", ""+ (event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDiv.style('display','none').style('opacity','0').style('background-color', '#f7f7f7');
            })

        //Total economía
        chart.selectAll('.circles')
            .data(data)
            .enter()
            .append('circle')
            .attr("r", 5)
            .attr("cx", function(d) { return x(d.mes_v2); })
            .attr("cy", function(d) { return y(d.total_economia); })
            .style("fill", '#000')
            .style('opacity', '0')
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('background-color', '#ffc8c8')
                    .style('display', 'block')
                    .style('opacity','0.9')
                    tooltipDiv.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><span style="font-weight: 900;">' + d.total_economia.toFixed(1).replace('.',',') + '%</span>')
                    .style("left", ""+ (event.pageX - 25) +"px")     
                    .style("top", ""+ (event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDiv.style('display','none').style('opacity','0').style('background-color', '#f7f7f7');
            })

        grafico.appendChild(divLeyenda);
    }
}

function getExpectativasPMI(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);   
    let lineaEjeX = proyecto == 'dashboard' ? !slideWidth ? 'area-expectativas-europa-ejeX' : 'slider-expectativas-europa-ejeX' : 'expectativas-europa-ejeX';
    
    //Nos quedamos con los países que sí dispongan de datos
    let newData = data.filter(function(item) {
        if(!isNaN(item.confianza)){
            return item;
        }
    });

    function comparativa(a,b){
        const datoA = a.confianza;
        const datoB = b.confianza;

        let comparacion = 0;
        if (datoA > datoB) {
            comparacion = -1;
        } else if (datoA <= datoB) {
            comparacion = 1;
        }
        return comparacion;
    }

    newData = newData.sort(comparativa);

    if(proyecto == 'dashboard'){
        !slideWidth ? 
        getChartExpectativasPMI(newData, contenedorGrafico, lineaEjeX, proyecto) : 
        getChartExpectativasPMI(newData, contenedorGrafico, lineaEjeX, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart2-5');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartExpectativasPMI(newData, grafico, lineaEjeX, proyecto);
    }
    
    function getChartExpectativasPMI(newData, grafico, lineaEjeX, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Estructura fundamental del gráfico
        let margin = {top: 10, right: 7.5, bottom: 92.5, left: 27.5},
        width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
        height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom;

        //Inicialización del gráfico
        let chart = select(grafico)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //Eje X
        let x = scaleBand()
            .range([0,width])
            .domain(range(newData.length))
            .paddingInner(.5)
            .align(1)

        let x2 = scaleBand()
            .range([0,width])
            .domain(newData.map(function(d) { return d.pais }))
            .paddingInner(.5)
            .align(1)

        let xAxis = function(g){
            g.call(axisBottom(x2))
            g.call(function(g){g.selectAll('.tick text').style('font-size', '13px').style('opacity','initial').attr('text-anchor','end').attr('transform', 'rotate(-90)').attr("dx", "-.8em").attr("dy", "0").attr("y", "3.5")})
            g.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {return d == 'España' ? '1' : '0.5'})})
            g.call(function(g){g.selectAll('.tick line').remove()})
            g.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','0')});
        }        

        chart.append('g')        
            .attr('transform', 'translate(0, ' + (height+10) + ')')
            .call(xAxis);
        
        //Eje Y
        let maxData = Math.ceil(max(data, function(d){ return +d.confianza })) > 0 ? Math.ceil(max(data, function(d){ return +d.confianza })) : 0;

        let y = scaleLinear()
            .domain([Math.floor(min(newData, function(d) {return d.confianza})), maxData])
            .range([height,0])
            .nice();

        let yAxis = function(g){
            g.call(axisLeft(y).ticks(6))
            g.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
            g.call(function(g){g.select('.domain').remove()})
            g.call(function(g){g.selectAll('.tick line')
                .attr('class', function(d) {return d == 0 && 'zero-base' })
                .attr("x1", '0%')
                .attr("x2", '' + (document.getElementById(lineaEjeX).getBoundingClientRect().width) + '')});
        }        

        chart.append('g')
            .call(yAxis);

        chart.selectAll('.barras')
            .data(newData)
            .enter()
            .append('rect')
            .attr('class', 'fill--unique')
            .attr('y', function(d) { return y(Math.max(0,d.confianza)) })
            .attr('x', function(d,i) { return x(i)})
            .attr('height', function(d) {return Math.abs(y(d.confianza) - y(0));})
            .attr('width', x.bandwidth())
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 900;">' + d.confianza.toFixed(1).replace('.',',') + '%</span>')
                    .style("left", ""+ (event.pageX - 25) +"px")     
                    .style("top", ""+ (event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDiv.style('display','none').style('opacity','0');
            })
    }
}

//Cuentas Públicas
function getCuentasDeficitHistorico(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);

    if(proyecto == 'dashboard'){
        !slideWidth ? 
        getChartCuentasDeficitHistorico(data, contenedorGrafico, proyecto) : 
        getChartCuentasDeficitHistorico(data, contenedorGrafico, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart3-1');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartCuentasDeficitHistorico(data, grafico, proyecto);
    }
    
    function getChartCuentasDeficitHistorico(data, grafico, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Estructura fundamental del gráfico
        let margin = {top: 7.5, right: 10, bottom: 17.75, left: 27.5},
        width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
        height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom;
        
        //Inicialización del gráfico
        let chart = select(grafico)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //Eje X
        let x = scaleBand()
            .range([0,width])
            .domain(range(data.length))
            .paddingInner(.75)
            .paddingOuter(.25)
            .align(.5);

        // let x2 = scaleTime()
        //     .domain(extent(data, function(d) {return formatMonthTime(d.mes)}))
        //     .range([0, width]);
        
        let xAxis = function(g){
            g.call(axisBottom(x).tickFormat(function(d){if(d == 0){return '2004'} else if (d == data.length - 1){return '2020'}}))
            g.call(function(g){g.selectAll('.tick line').remove()})
            g.call(function(g){g.select('.domain').attr('id','domain-cuentas-deficit-' + proyecto == 'dashboard' ? !slideWidth ? 'area' : 'slider' : 'panel')})
        }        
        
        chart.append('g')
            .attr('transform', 'translate(0, ' + height + ')')
            .call(xAxis);

        //Eje Y
        let y = scaleLinear()
            .domain([Math.floor(min(data, function(d){ return +d.deficit })), Math.ceil(max(data, function(d){ return +d.deficit }))])
            .range([height,0]);

        let yAxis = function(g){
            g.call(axisLeft(y).ticks(6))
            g.call(function(g){g.selectAll('.tick text').attr('class', function(d){ return d == 0 && 'tick-text-special'})})
            g.call(function(g){g.select('.domain').remove()})
            g.call(function(g){g.selectAll('.tick line')
                .attr('class', function(d) {return d == 0 && 'zero-base' }) //No estaba indicado el d == 0
                .attr("x1", '0%')
                .attr("x2", '' + (document.getElementById('domain-cuentas-deficit-' + proyecto == 'dashboard' ? !slideWidth ? 'area' : 'slider' : 'panel').getBoundingClientRect().width) + '')});
        }        
        
        chart.append('g')
            .call(yAxis);

        //Visualización
        chart.selectAll('.barras')
            .data(data)
            .enter()
            .append('rect')
            .attr('class', 'fill--unique')
            .attr('y', function(d) { return y(Math.max(0,d.deficit)) })
            .attr('x', function(d,i) { return x(i)})
            .attr('height', function(d) {return Math.abs(y(d.deficit) - y(0));})
            .attr('width', x.bandwidth())
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes) + '</span><span style="font-weight: 900;">' + d.deficit.toFixed(2).replace('.',',') + '%</span>')
                    .style("left", ""+ (event.pageX - 25) +"px")     
                    .style("top", ""+ (event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDiv.style('display','none').style('opacity','0');
            })
    }        
}

function getCuentasDeudaPublica(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    let lineaEjeX = proyecto == 'dashboard' ? !slideWidth ? 'area-cuentas-deuda-ejeX' : 'slider-cuentas-deuda-ejeX' : 'cuentas-deuda-ejeX';

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartCuentasDeudaPublica(data, contenedorGrafico, lineaEjeX, proyecto) : 
            getChartCuentasDeudaPublica(data, contenedorGrafico, lineaEjeX, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart3-2');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartCuentasDeudaPublica(data, grafico, lineaEjeX, proyecto);
    }

    function getChartCuentasDeudaPublica(data, grafico, lineaEjeX, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Estructura fundamental del gráfico
        let margin = {top: 7.5, right: 10, bottom: 17.75, left: 27.5},
        width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
        height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom;
        
        //Inicialización del gráfico
        let chart = select(grafico)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //Eje X
        let x = scaleBand()
            .range([0,width])
            .domain(range(data.length));

        let x2 = scaleTime()
            .range([0,width])
            .domain(extent(data, function(d) { return formatTimeV2(d.trimestre, "cuentas_publicas") }));

        let xAxis = function(g){
            g.call(axisBottom(x2))
            g.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','0')})
            g.call(function(g){g.selectAll('.tick line').remove()})
            g.call(function(g){g.selectAll('.tick').attr('opacity', function(d){return d.getFullYear() == 2020 || d.getFullYear() == 1996 || d.getFullYear() == 2004 || d.getFullYear() == 2012 ? '1' : '0'})})
            g.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d){return d.getFullYear() == 2020 ? 'end' : 'middle'})});        
        }        

        chart.append('g')
            .attr('transform', 'translate(0, ' + height + ')')
            .call(xAxis);
        
        //Eje Y
        let y0 = Math.max(Math.abs(min(data, function(d) {return d.porcentaje_deuda})), Math.abs(max(data, function(d) {return d.porcentaje_deuda})));

        let y = scaleLinear()
            .domain([0,y0])
            .range([height,0])
            .nice();

        let yAxis = function(g){
            g.call(axisLeft(y).ticks(6))
            g.call(function(g){g.selectAll('.tick text').attr('class', function(d){ return d == 0 && 'tick-text-special'})})
            g.call(function(g){g.select('.domain').remove()})
            g.call(function(g){g.selectAll('.tick line')
                .attr('class', function(d) {return d == 0 && 'zero-base' }) //No estaba indicado el d == 0
                .attr("x1", '0%')
                .attr("x2", '' + (document.getElementById(lineaEjeX).getBoundingClientRect().width) + '')});
        }        

        chart.append('g')
            .call(yAxis);

        //Visualización
        chart.selectAll('.barras')
            .data(data)
            .enter()
            .append('rect')
            .attr('class', 'fill--unique')
            .attr('y', function(d) { return y(Math.max(0,d.porcentaje_deuda)) })
            .attr('x', function(d,i) { return x(i)})
            .attr('height', function(d) {return Math.abs(y(d.porcentaje_deuda) - y(0));})
            .attr('width', x.bandwidth() / 2)
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 300;">' + formatTooltipDate(d.trimestre) + '</span><span style="font-weight: 900;">' + d.porcentaje_deuda.toFixed(2).replace('.',',') + '%</span>')
                    .style("left", ""+ (event.pageX - 25) +"px")     
                    .style("top", ""+ (event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDiv.style('display','none').style('opacity','0');
            })
    }
}

function getCuentasDeficitCCAA(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartCuentasDeficitCCAA(data, contenedorGrafico, proyecto) : 
            getChartCuentasDeficitCCAA(data, contenedorGrafico, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart3-4');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartCuentasDeficitCCAA(data, grafico, proyecto);
    }

    function getChartCuentasDeficitCCAA(data, grafico, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Creación de leyenda
        //let contenedor = proyecto == 'dashboard' ? document.getElementById(grafico.substr(1)) : grafico;    
        let divLeyenda = document.createElement('div');
        divLeyenda.setAttribute('class','legend');

        let primerPunto = document.createElement('p');
        primerPunto.setAttribute('class','legend__item legend__item--primero');
        primerPunto.textContent = 'Menos de -0,5';

        let segundoPunto = document.createElement('p');
        segundoPunto.setAttribute('class','legend__item legend__item--segundo');
        segundoPunto.textContent = '-0,5 a 0';

        let tercerPunto = document.createElement('p');
        tercerPunto.setAttribute('class','legend__item legend__item--tercero');
        tercerPunto.textContent = '-0 a 0,5';

        let cuartoPunto = document.createElement('p');
        cuartoPunto.setAttribute('class','legend__item legend__item--cuarto');
        cuartoPunto.textContent = 'Más de 0,5';    

        divLeyenda.appendChild(primerPunto);
        divLeyenda.appendChild(segundoPunto);
        divLeyenda.appendChild(tercerPunto);
        divLeyenda.appendChild(cuartoPunto);

        json(mapaAutonomias, function(error, mapa) {
            if (error) throw error;
            //No hay datos para Ceuta y Melilla, por lo que no es necesario recuperarlas
            let coordenadasMapa = feature(mapa, mapa.objects.autonomous_regions);
            let datosMapa = coordenadasMapa.features;

            data.forEach(function(item){
                datosMapa.forEach(function(item2){ //Sólo quedan sin datos Ceuta y Melilla
                    if(parseInt(item.id_ccaa) == parseInt(item2.id)){
                        item2.ccaa = item.ccaa;
                        item2.deficit_acumulado = item.deficit_acumulado;
                    }
                })
            });    
            
            //Estructura fundamental del gráfico
            let width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth),
            height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - 25;
                
            //Inicialización del gráfico
            let chart = select(grafico)
                .append('svg')
                .attr('width', width)
                .attr('height', height);

            const projection = geoConicConformalSpain();
            const path = geoPath(projection.fitSize([width,height], coordenadasMapa));

            chart.selectAll(".region")
                .data(datosMapa)
                .enter()
                .append("path")
                .attr('id', function(d){return d.ccaa})
                .attr("d", path)
                .attr("class", function(d){return d.deficit_acumulado < -0.5 ? 'stroke-in-maps map-fill--worst' : d.deficit_acumulado < 0 && d.deficit_acumulado >= -0.5 ? 'stroke-in-maps map-fill--worse' : d.deficit_acumulado < 0.5 && d.deficit_acumulado >= 0 ? 'stroke-in-maps map-fill--better' : 'stroke-in-maps map-fill--best'})
                .on('touchstart mouseover', function(d){
                    tooltipDiv.transition()     
                        .duration(200)
                        .style('display', 'block')
                        .style('opacity','0.9')
                    tooltipDiv.html('<span style="font-weight: 300;">' + d.ccaa + '</span><span style="font-weight: 900;">' + d.deficit_acumulado.toFixed(2).replace('.',',') + '%</span>')
                        .style("left", ""+ (event.pageX - 25) +"px")     
                        .style("top", ""+ (event.pageY - 35) +"px");
                })
                .on('touchend mouseout mouseleave', function(d){
                    tooltipDiv.style('display','none').style('opacity','0');
                });

                grafico.appendChild(divLeyenda);
        });

        
    }
}

//Empleo
function getEmpleoAfiliacionHistorico(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){    
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    let lineaEjeX = proyecto == 'dashboard' ? !slideWidth ? 'area-empleo-afiliacion-historico-ejeX' : 'slider-empleo-afiliacion-historico-ejeX' : 'empleo-afiliacion-historico-ejeX';

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartEmpleoAfiliacionHistorico(data, contenedorGrafico, lineaEjeX, proyecto) : 
            getChartEmpleoAfiliacionHistorico(data, contenedorGrafico, lineaEjeX, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart4-2');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartEmpleoAfiliacionHistorico(data, grafico, lineaEjeX, proyecto);
    }    
    
    function getChartEmpleoAfiliacionHistorico(data, grafico, lineaEjeX, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Estructura fundamental del gráfico
        let margin = {top: 5, right: 7.5, bottom: 17.75, left: 55},
        width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
        height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom;

        //Inicialización del gráfico
        let chart = select(grafico)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //Eje X
        let x = scaleTime()
            .domain(extent(data, function(d) {return formatDayTime(d.fecha)}))
            .range([0, width]);
        
        let xAxis = function(g){
            g.call(axisBottom(x).tickValues(x.domain().filter(function(d,i) { return !(i%1) })).tickFormat(function(d){ return meses[d.getMonth()];}))
            g.call(function(g){g.selectAll('.tick line').remove()})
            g.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','1')})
            g.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d,i) {return i == 0 ? 'start' : 'end'})});
        }

        chart.append('g')        
            .attr('transform', 'translate(0, ' + height + ')')
            .call(xAxis);

        //Eje Y
        let y = scaleLinear()
            .domain([min(data, function(d){ return d.afiliados - 100000 }),19500000])
            .range([height,0]);

        let yAxis = function(g){
            g.call(axisLeft(y).ticks(4).tickFormat(format(".3s")))
            g.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
            g.call(function(g){g.selectAll('.tick line')
                .attr('x2','0')
                .attr('x1','' + (document.getElementById(lineaEjeX).getBoundingClientRect().width) + '')
                .attr('class', function(d) {return d == 0 && 'zero-base'}) //No tenía el d == 0
                //.style('opacity', function(d) {return d == 0 ? '1' : '0'})
            });
        }
        
        chart.append("g")
            .attr('transform', 'translate(0, 0)')
            .call(yAxis);

        //Line
        let line = d3line()
            .x(function(d){ return x(formatDayTime(d.fecha))})
            .y(function(d){return y(d.afiliados)});
        
        chart.append("path")
            .datum(data)
            .attr('class', 'area-lines')
            .attr('transform', 'translate(0, 0)')
            .attr("d", line);

        //Area > set the gradient
        chart.append("linearGradient")				
        .attr("id", proyecto == 'dashboard' ? !slideWidth ? 'area-empleo-afiliacion' : 'slider-empleo-afiliacion' : 'empleo-afiliacion')			
        .attr("gradientUnits", "userSpaceOnUse")	
        .attr("x1", 0).attr("y1", y(min(data, function(d) {return d.afiliados})))			
        .attr("x2", 0).attr("y2", y(max(data, function(d) {return d.afiliados})))		
        .selectAll("stop")						
        .data([
            {offset: "0%", color: "rgba(255, 70, 70, 0)"},
            {offset: "80%", color: "rgba(255, 70, 70, 0.4)"},
            {offset: "100%", color: "rgba(255, 70, 70, 1)"}	
        ])					
        .enter()
        .append("stop")			
        .attr("offset", function(d) { return d.offset; })	
        .attr("stop-color", function(d) { return d.color; });

        let area = d3area()
            .x(function(d) { return x(formatDayTime(d.fecha)) })	
            .y0(height)					
            .y1(function(d) { return y(d.afiliados); });

        chart.append('path')
            .datum(data)
            .attr('fill', proyecto == 'dashboard' ? !slideWidth ? 'url(#area-empleo-afiliacion)' : 'url(#slider-empleo-afiliacion)' : 'url(#empleo-afiliacion)')
            .attr('d', area);

        //Dibujo círculos
        chart.selectAll('.circles')
            .data(data)
            .enter()
            .append('circle')
            .attr("r", 5)
            .attr("cx", function(d) { return x(formatDayTime(d.fecha)) })
            .attr("cy", function(d) { return y(d.afiliados); })
            .style("fill", '#000')
            .style('opacity', '0')
            .attr('transform', 'translate(2.5, 0)')
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 300;">' + formatTooltipDate(d.fecha) + '</span><span style="font-weight: 900;">' + getNumberWithCommas(d.afiliados) + '</span>')
                    .style("left", ""+ (event.pageX - 25) +"px")     
                    .style("top", ""+ (event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDiv.style('display','none').style('opacity','0');
            })
    }
}

function getEmpleoTasaParoHistorica(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    let lineaEjeX = proyecto == 'dashboard' ? !slideWidth ? 'area-empleo-tasa-paro-historica-ejeX' : 'slider-empleo-tasa-paro-historica-ejeX' : 'empleo-tasa-paro-historica-ejeX';

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartEmpleoTasaParoHistorica(data, contenedorGrafico, lineaEjeX, proyecto) : 
            getChartEmpleoTasaParoHistorica(data, contenedorGrafico, lineaEjeX, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart4-3');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartEmpleoTasaParoHistorica(data, grafico, lineaEjeX, proyecto);
    }   

    function getChartEmpleoTasaParoHistorica(data, grafico, lineaEjeX, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Estructura fundamental del gráfico
        let margin = {top: 12.5, right: 7.5, bottom: 17.75, left: 27.5},
        width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
        height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom;

        //Inicialización del gráfico
        let chart = select(grafico)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //Eje X
        let x = scaleBand()
            .range([0,width])
            .domain(range(data.length));

        let x2 = scaleTime()
            .range([0,width])
            .domain(extent(data, function(d) { return formatTime(d.trimestre_v2) }));

        let xAxis = function(g){
            g.call(axisBottom(x2).ticks(18).tickFormat(function(d) { return d.getFullYear(); }))
            g.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {if(d.getFullYear() == 2002 || d.getFullYear() == 2008 || d.getFullYear() == 2013 ||d.getFullYear() == 2020){return 1} else {return 0}})})
            g.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d) {return d.getFullYear() == 2020 ? 'end' : d.getFullYear() == 2002 ? 'start' : 'middle' })})
            g.call(function(g){g.selectAll('line').remove()})
            g.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','0')});
        }        

        chart.append('g')
            .attr('transform', 'translate(0, ' + height + ')')
            .call(xAxis);
        
        //Eje Y
        //let y0 = Math.max(Math.abs(min(data, function(d) {return d.tasa_paro})), Math.abs(max(data, function(d) {return d.tasa_paro})));

        let y = scaleLinear()
            .domain([0,30])
            .range([height,0])
            .nice();

        let yAxis = function(g){
            g.call(axisLeft(y).ticks(7))
            g.call(function(g){g.selectAll('.tick text').attr('class', function(d) { return d == 0 && 'tick-text-special'})})
            g.call(function(g){g.select('.domain').remove()})
            g.call(function(g){g.selectAll('.tick line')
                .attr('class', function(d){return d == 0 && 'zero-base'})
                .attr("x1", '0%')
                .attr("x2", '' + (document.getElementById(lineaEjeX).getBoundingClientRect().width) + '')});
        }        

        chart.append('g')
            .attr("transform", 'translate(0,0)')
            .call(yAxis);

        chart.selectAll('.barras')
            .data(data)
            .enter()
            .append('rect')
            .attr('class', 'fill--unique')
            .attr('y', function(d) { return y(Math.max(0,d.tasa_paro)) })
            .attr('x', function(d,i) { return x(i)})
            .attr('height', function(d) {return Math.abs(y(d.tasa_paro) - y(0));})
            .attr('width', x.bandwidth() / 2)
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 300;">' + d.trimestre + '</span><span style="font-weight: 900;">' + d.tasa_paro.toFixed(2).replace('.',',') + '%</span>')
                    .style("left", ""+ (event.pageX - 25) +"px")     
                    .style("top", ""+ (event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDiv.style('display','none').style('opacity','0');
            })
    }
}

function getEmpleoNumeroParados(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    
    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartEmpleoNumeroParados(data, contenedorGrafico, proyecto) : 
            getChartEmpleoNumeroParados(data, contenedorGrafico, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart4-4');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartEmpleoNumeroParados(data, grafico, proyecto);
    }

    function getChartEmpleoNumeroParados(data, grafico, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Estructura fundamental del gráfico
        let margin = {top: 12.5, right: 7.5, bottom: 17.75, left: 27.5},
        width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
        height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom;
        
        //Inicialización del gráfico
        let chart = select(grafico)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //Eje X
        let x = scaleTime()
            .domain(extent(data, function(d) {return formatTimeV2(d.mes_v2, "mes en letras")}))
            .range([0, width]);
        
        let xAxis = function(g){
            g.call(axisBottom(x))
            g.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {if(d.getFullYear() == 1996 || d.getFullYear() == 2004 || d.getFullYear() == 2012 || d.getFullYear() == 2020){return 1} else {return 0}})})
            g.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d) {return d.getFullYear() == 2020 ? 'end' : d.getFullYear() == 1996 ? 'start' : 'middle' })})
            g.call(function(g){g.selectAll('.tick line').remove()});
        }
        
        chart.append('g')
            .attr('transform', 'translate(0, ' + height + ')')
            .call(xAxis);

        //Eje Y
        let y = scaleLinear()
            .domain([0, Math.ceil(max(data, function(d) {return d.paro_registrado}))])
            .range([height,0]);

        let yAxis = function(g){
            g.attr("transform", 'translate(0,0)')
            g.call(axisLeft(y).ticks(5).tickFormat(format(".0s")))
            g.call(function(g){g.selectAll('.tick text').attr('class', function(d) { return d == 0 && 'tick-text-special'})})
            g.call(function(g){g.selectAll('.tick line').remove()});}
        
        chart.append("g")
            .attr('transform', 'translate(0, 0)')
            .call(yAxis);

        //Line
        let line = d3line()
            .x(function(d){return x(formatTimeV2(d.mes_v2, "mes en letras"))})
            .y(function(d){return y(d.paro_registrado)});
        
        chart.append("path")
            .datum(data.filter(line.defined()))
            .attr('class', 'stroke--unique')
            .attr('transform', 'translate(0, 0)')
            .attr("d", line);

        //Dibujo círculos
        chart.selectAll('.circles')
            .data(data)
            .enter()
            .append('circle')
            .attr("r", 5)
            .attr("cx", function(d) { return x(formatTimeV2(d.mes_v2, "mes en letras")); })
            .attr("cy", function(d) { return y(d.paro_registrado); })
            .style("fill", '#000')
            .style('opacity', '0')
            .attr('transform', 'translate(2.5, 0)')
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><span style="font-weight: 900;">' + getNumberWithCommas(d.paro_registrado) + '</span>')
                    .style("left", ""+ (event.pageX - 65) +"px")     
                    .style("top", ""+ (event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDiv.style('display','none').style('opacity','0');
            })
    }
}

function getEmpleoTasaParoEuropa(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    let lineaEjeX = proyecto == 'dashboard' ? !slideWidth ? 'area-empleo-tasa-paro-europa-ejeX' : 'slider-empleo-tasa-paro-europa-ejeX' : 'empleo-tasa-paro-europa-ejeX';

    //Nos quedamos con los países que sí dispongan de datos
    let newData = data.filter(function(item) {
        if(!isNaN(item.datos_paro)){
            return item;
        }
    });

    function comparativa(a,b){
        const datoA = a.datos_paro;
        const datoB = b.datos_paro;

        let comparacion = 0;
        if (datoA > datoB) {
            comparacion = 1;
        } else if (datoA <= datoB) {
            comparacion = -1;
        }
        return comparacion;
    }
    newData = newData.sort(comparativa);

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartEmpleoTasaParoEuropa(newData, contenedorGrafico, lineaEjeX, proyecto) : 
            getChartEmpleoTasaParoEuropa(newData, contenedorGrafico, lineaEjeX, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart4-6');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartEmpleoTasaParoEuropa(newData, grafico, lineaEjeX, proyecto);
    }

    function getChartEmpleoTasaParoEuropa(newData, grafico, lineaEjeX, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Estructura fundamental del gráfico
        let margin = {top: 12.5, right: 12.5, bottom: 50, left: 27.5},
        width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
        height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom;

        //Inicialización del gráfico
        let chart = select(grafico)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //Eje X
        let x = scaleBand()
            .domain(newData.map(function(d){return d.pais}))
            .range([0, width])
            .paddingInner(.5)
            .align(1)

        let xAxis = function(g){
            g.call(axisBottom(x))
            g.call(function(g){g.selectAll('.tick line').remove()})
            g.call(function(g){g.selectAll('.tick text').style('font-size', '13px').style('opacity','initial').attr('text-anchor','end').attr('transform', 'rotate(-90)').attr("dx", "-.8em").attr("dy", "0").attr("y", "3.5")})
            g.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {return d == 'España' ? '1' : '0.5'})})
            g.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','0')});
        }        

        chart.append('g')
            .attr("transform", "translate(0," + (height - 50) + ")")
            .call(xAxis);

        //Eje Y
        let maxData = Math.ceil(max(data, function(d){ return +d.datos_paro})) > 20 ? Math.ceil(max(data, function(d){ return +d.datos_paro})) : 20;
        let y = scaleLinear()
            .range([height - 57.5,0])
            .domain([0,maxData]);

        let yAxis = function(g){
            g.call(axisLeft(y).ticks(5))
            g.call(function(g){g.selectAll('.tick text').attr('class', function(d) { return d == 0 && 'tick-text-special'})})
            g.call(function(g){g.select('.domain').remove()})
            g.call(function(g){g.selectAll('.tick line')
                .attr('class', function(d) {return d == 0 && 'zero-base'})
                .attr("x1", '0%')
                .attr("x2", '' + (document.getElementById(lineaEjeX).getBoundingClientRect().width) + '')});
        }        
        
        chart.append('g')
            .attr('transform', 'translate(0,0)')
            .call(yAxis);

        //Creación de barras
        chart.selectAll("bar")
            .data(newData)
            .enter()
            .append("rect")
            .attr('class', 'fill--unique')
            .attr("x", function(d) { return x(d.pais); })
            .attr("width", x.bandwidth())
            .attr("y", function(d) {return !isNaN(d.datos_paro) ? y(d.datos_paro) : y(0); })
            .attr("height", function(d) { return (height - 57.5) - y(d.datos_paro); })
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 900;">' + d.datos_paro.toFixed(2).replace('.',',') + '%</span>')
                    .style("left", ""+ (event.pageX - 25) +"px")     
                    .style("top", ""+ (event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDiv.style('display','none').style('opacity','0');
            })
    }    
}

//Demanda
function getDemandaHistorica(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartDemandaHistorica(data, contenedorGrafico, proyecto) : 
            getChartDemandaHistorica(data, contenedorGrafico, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart5-1');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartDemandaHistorica(data, grafico, proyecto);
    }

    function getChartDemandaHistorica(data, grafico, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Estructura fundamental del gráfico
        let margin = {top: 7.5, right: 7.5, bottom: 17.75, left: 27.5},
        width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
        height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom;
        
        //Inicialización del gráfico
        let chart = select(grafico)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //Eje X
        let x = scaleTime()
            .domain(extent(data, function(d) {return formatTimeV2(d.mes_v2, "mes en letras")}))
            .range([0, width]);
        
        let xAxis = function(g){
            g.call(axisBottom(x))
            g.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {if(d.getFullYear() == 2000 || d.getFullYear() == 2006 || d.getFullYear() == 2014 || d.getFullYear() == 2020){return 1} else {return 0}})})
            g.call(function(g){g.selectAll('.tick text').attr('text-anchor', function(d) {return d.getFullYear() == 2020 ? 'end' : d.getFullYear() == 2000 ? 'start' : 'middle' })})
            g.call(function(g){g.selectAll('.tick line').remove()});
        }        
        
        chart.append('g')
            .attr('transform', 'translate(0, ' + height + ')')
            .call(xAxis);

        //Eje Y
        let maxData = Math.ceil(data, function(d){ return +d.indice_precios_corrientes }) > 125 ? Math.ceil(data, function(d){ return +d.indice_precios_corrientes }) : 125;
        let y = scaleLinear()
            .domain([0,maxData])
            .range([height,0]);

        let yAxis = function(g){
            g.attr("transform", 'translate(0,0)')
            g.call(axisLeft(y).ticks(6))
            g.call(function(g){g.selectAll('.tick text').attr('class', function(d) { return d == 0 && 'tick-text-special'})})
            g.call(function(g){g.selectAll('.tick line').remove()});
        }        
        
        chart.append("g")
            .attr('transform', 'translate(0, 0)')
            .call(yAxis);

        //Line
        let line = d3line()
            .x(function(d){return x(formatTimeV2(d.mes_v2, "mes en letras"))})
            .y(function(d){return y(d.indice_precios_corrientes)});
        
        chart.append("path")
            .datum(data.filter(line.defined()))
            .attr('class', 'stroke--unique')
            .attr('transform', 'translate(0, 0)')
            .attr("d", line);

        //Dibujo círculos
        chart.selectAll('.circles')
            .data(data)
            .enter()
            .append('circle')
            .attr("r", 5)
            .attr("cx", function(d) { return x(formatTimeV2(d.mes_v2, "mes en letras")); })
            .attr("cy", function(d) { return y(d.indice_precios_corrientes); })
            .style("fill", '#000')
            .style('opacity', '0')
            .attr('transform', 'translate(2.5, 0)')
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 300;">' + formatTooltipDate(d.mes_v2) + '</span><span style="font-weight: 900;">' + d.indice_precios_corrientes.toFixed(2).replace('.',',') + '</span>')
                    .style("left", ""+ (event.pageX - 25) +"px")     
                    .style("top", ""+ (event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDiv.style('display','none').style('opacity','0');
            });
    }
}

function getDemandaCCAA(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartDemandaCCAA(data, contenedorGrafico, proyecto) : 
            getChartDemandaCCAA(data, contenedorGrafico, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart5-3');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartDemandaCCAA(data, grafico, proyecto);
    }

    function getChartDemandaCCAA(data, grafico, proyecto, slideWidth = undefined, slideHeight = undefined){ 
        //Creación de leyenda        
        let divLeyenda = document.createElement('div');
        divLeyenda.setAttribute('class','legend');

        let primerPunto = document.createElement('p');
        primerPunto.setAttribute('class','legend__item legend__item--primero');
        primerPunto.textContent = 'Menos de -10';

        let segundoPunto = document.createElement('p');
        segundoPunto.setAttribute('class','legend__item legend__item--segundo');
        segundoPunto.textContent = '-10 a -5';

        let tercerPunto = document.createElement('p');
        tercerPunto.setAttribute('class','legend__item legend__item--tercero');
        tercerPunto.textContent = '-5 a 0';

        let cuartoPunto = document.createElement('p');
        cuartoPunto.setAttribute('class','legend__item legend__item--cuarto');
        cuartoPunto.textContent = 'Más de 0';

        divLeyenda.appendChild(primerPunto);
        divLeyenda.appendChild(segundoPunto);
        divLeyenda.appendChild(tercerPunto);
        divLeyenda.appendChild(cuartoPunto);

        //Estructura fundamental del gráfico
        let width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth);
        let height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - 25;
        
        //Inicialización del gráfico
        let chart = select(grafico)
            .append('svg')
            .attr('width', width)
            .attr('height', height);

        function initMap(){
            json(mapaAutonomias, function (error, mapa) {
                if (error) throw error;
                //No hay datos para Ceuta y Melilla, por lo que no es necesario recuperarlas
                let coordenadasMapa = feature(mapa, mapa.objects.autonomous_regions);
                let datosMapa = coordenadasMapa.features;
        
                data.forEach(function(item) {
                    datosMapa.forEach(function(item2){ //Sólo quedan sin datos Ceuta y Melilla
                        if(parseInt(item.id_ccaa) == parseInt(item2.id)){
                            item2.ccaa = item.ccaa;
                            item2.variacion_anual = item.variacion_anual;
                            item2.variacion_mensual = item.variacion_mensual;
                        }
                    })
                });
                
                const projection = geoConicConformalSpain();
                const path = geoPath(projection.fitSize([width,height], coordenadasMapa));
        
                chart.selectAll(".region")
                    .data(datosMapa)
                    .enter()
                    .append("path")
                    .attr('id', function(d) {return d.ccaa})
                    .attr("d", path)
                    .attr('class', function(d){return d.variacion_anual < -10 ? 'stroke-in-maps map-fill--worst' : d.variacion_anual < -5 && d.variacion_anual >= -10 ? 'stroke-in-maps map-fill--worse' : d.variacion_anual <= 0 && d.variacion_anual >= -5 ? 'stroke-in-maps map-fill--better' : 'stroke-in-maps map-fill--best'})
                    .on('touchstart mouseover', function(d) {
                        tooltipDiv.transition()     
                            .duration(200)
                            .style('display', 'block')
                            .style('opacity','0.9')
                        tooltipDiv.html('<span style="font-weight: 300;">' + d.ccaa + '</span><span style="font-weight: 900;">' + d.variacion_anual.toFixed(2).replace('.',',') + '%</span>')
                            .style("left", ""+ (event.pageX - 25) +"px")     
                            .style("top", ""+ (event.pageY - 35) +"px");
                    })
                    .on('touchend mouseout mouseleave', function(d) {
                        tooltipDiv.style('display','none').style('opacity','0');
                    })

                grafico.appendChild(divLeyenda);
            });
        }

        initMap();
    }        
}

function getDemandaEurozona(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    let lineaEjeX = proyecto == 'dashboard' ? !slideWidth ? 'area-demanda-eurozona-ejeX' : 'slider-demanda-eurozona-ejeX' : 'demanda-eurozona-ejeX';
    
    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartDemandaEurozona(data, contenedorGrafico, lineaEjeX, proyecto) : 
            getChartDemandaEurozona(data, contenedorGrafico, lineaEjeX, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart5-4');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartDemandaEurozona(data, grafico, lineaEjeX, proyecto);
    }

    function getChartDemandaEurozona(data, grafico, lineaEjeX, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Estructura fundamental del gráfico
        let margin = {top: 7.5, right: 12.5, bottom: 84.75, left: 27.5},
        width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
        height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom;
                
        //let newData = data.filter((item) => {return item.consumo.trim() != ""});

        //newData = newData.sort(comparativa);
        
        function comparativa(a,b){
            const datoA = +a.consumo;
            const datoB = +b.consumo;

            let comparacion = 0;
            if (datoA < datoB) {
                comparacion = 1;
            } else if (datoA >= datoB) {
                comparacion = -1;
            }
            return comparacion;
        }

        let newData = data.sort(comparativa);
        //console.log(newData);

        //Inicialización del gráfico
        let chart = select(grafico)
            .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
            .append('g')
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //Eje X
        let x = scaleBand()
            .range([0,width])
            .domain(range(newData.length));

        let x2 = scaleBand()
            .range([0,width])
            .domain(newData.map(function(d) { return d.pais }))

        let xAxis = function(g){
            g.call(axisBottom(x2))
            g.call(function(g){g.selectAll('.tick text').style('font-size', '13px').style('opacity','initial').attr('text-anchor','end').attr('transform', 'rotate(-90)').attr('y', - ((x2.bandwidth() / 2)  - 6.5))})
            g.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {return d == 'España' ? '1' : '0.5'})})
            g.call(function(g){g.selectAll('.tick line').remove()})
            g.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','0')});
        }        

        chart.append('g')
            .attr('transform', 'translate(0, ' + (height+10) + ')')
            .call(xAxis);
        
        //Eje Y
        let y = scaleLinear()
            .domain([Math.floor(min(newData, function(d) {return +d.consumo})), Math.ceil(max(newData, function(d) {return +d.consumo}))])
            .range([height,0])
            .nice();

        let yAxis = function(g){
            g.call(axisLeft(y).ticks(6))
            g.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
            g.call(function(g){g.select('.domain').remove()})
            g.call(function(g){g.selectAll('.tick line')
                .attr('class', function(d) {return d == 0 && 'zero-base'})
                .attr("x1", '0%')
                .attr("x2", '' + (document.getElementById(lineaEjeX).getBoundingClientRect().width) + '')});
        }        

        chart.append('g')
            .call(yAxis);

        chart.selectAll('.barras')
            .data(newData)
            .enter()
            .append('rect')
            .attr('class', 'fill--unique')
            .attr('y', function(d) { return y(Math.max(0,+d.consumo)) })
            .attr('x', function(d,i) {let position = x(i) + x.bandwidth() / 4; return position;})
            .attr('height', function(d) {return Math.abs(y(+d.consumo) - y(0));})
            .attr('width', x.bandwidth() / 2)
            .on('touchstart mouseover', function(d) {
                tooltipDiv.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 900;">' + d.consumo.toFixed(2).replace('.',',') + '%</span>')
                    .style("left", ""+ (event.pageX - 25) +"px")     
                    .style("top", ""+ (event.pageY - 35) +"px");
            })
            .on('touchend mouseout mouseleave', function(d) {
                tooltipDiv.style('display','none').style('opacity','0');
            })
    }
}

//Sectores
function getSectoresCambioAfiliacion(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){ //Gráfico lollipop separado a la mitad
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartSectoresCambioAfiliacion(data, contenedorGrafico, proyecto) : 
            getChartSectoresCambioAfiliacion(data, contenedorGrafico, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart6-1');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartSectoresCambioAfiliacion(data, grafico, proyecto);
    }
    
    function getChartSectoresCambioAfiliacion(data, grafico, proyecto, slideWidth = undefined, slideHeight = undefined){
        //Estructura fundamental del gráfico
        let margin = {top: 2.5, right: 25, bottom: 17.75, left: 110};
        let width = grafico.clientWidth - margin.left - margin.right;
        let height = grafico.clientHeight - margin.top - margin.bottom;
        
        //Valores para eje X
        let x, xAxis;
        let tipo = 'porcentaje';

        let newData = [];
        for(let i = 0; i < data.length; i++){
            if(data[i].sector_mostrar == 'S. primario' || data[i].sector_mostrar == 'Manufacturera' || data[i].sector_mostrar == 'Construcción' ||
            data[i].sector_mostrar == 'C. minorista' || data[i].sector_mostrar == 'Transporte' || data[i].sector_mostrar == 'Hostelería' ||
            data[i].sector_mostrar == 'S. público' || data[i].sector_mostrar == 'Educación' || data[i].sector_mostrar == 'Sanidad' ||
            data[i].sector_mostrar == 'Ocio'){
                newData.push(data[i]);
            }
        }

        //Funciones de visualización de datos       
        function unicoGrafico(newData, width){
            let svg = select(grafico)
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

            //X axis
            x = scaleLinear()
                .domain([Math.floor(min(newData, function(d){ return +d.data_porcentaje })), Math.ceil(max(newData, function(d){ return +d.data_porcentaje }))])
                .range([0, width]);
        
            xAxis = function(g){
                g.call(axisBottom(x).ticks(4))
                g.call(function(g){g.selectAll('.domain').remove()})
                g.call(function(g){g.selectAll('.tick line').remove()})
                g.attr("transform", "translate(0," + (height) + ")");
            }

            svg.append("g")
                .attr('class', 'x-axis')
                .call(xAxis);

            // Y axis
            let y = scaleBand()
            .range([0, height])
            .domain(newData.map(function(d) { return d.sector_mostrar; }))
            .paddingOuter(0);

            let yAxis = function(g){
                g.call(axisLeft(y))
                g.call(function(g){g.selectAll('.domain').remove()})
                g.call(function(g){g.selectAll('.tick text').style('font-size', '13px').style('opacity','1').style('font-weight','300')})
                g.call(function(g){g.selectAll('.tick line')
                    .attr('x2','0')
                    .attr('x1','100%')
                    .attr('class', function(d) {return d == 0 && 'zero-base'}) //No tenía el d == 0
                    .style('stroke-dasharray','2')});
            }            

            svg.append("g")
                .call(yAxis);

            // Line >> With gradient
            svg.selectAll("myline")
            .data(newData)
            .enter()
            .append("line")
                .attr("x1", function(d) { return x(0); })
                .attr("x2", function(d) { return x(d.data_porcentaje); })
                .attr("y1", function(d) { return y(d.sector_mostrar) + (y.bandwidth() / 2); })
                .attr("y2", function(d) { return y(d.sector_mostrar) + (y.bandwidth() / 2); })
                .attr('class', 'my-lines lollipop-lines');

            // Circles of variable 1
            svg.selectAll("mycircle")
                .data(newData)
                .enter()
                .append("circle")
                    .attr("cx", function(d) { return x(0); })
                    .attr("cy", function(d) { return y(d.sector_mostrar) + (y.bandwidth() / 2); })
                    .attr("r", "3")
                    .attr('class', 'my-circles-1 lollipop-zero-circles');

            //Circles of 2
            svg.selectAll("mycircle")
                .data(newData)
                .enter()
                .append("circle")
                    .attr("class", "my-circles-2 fill--unique")
                    .attr("cx", function(d) { return x(d.data_porcentaje); })
                    .attr("cy", function(d) { return y(d.sector_mostrar) + (y.bandwidth() / 2); })
                    .attr("r", "3")
                    .on('touchstart mouseover', function(d) {
                        let dato = '';
                        if(tipo == 'porcentaje'){
                            dato = getNumberWithCommas(d.data_porcentaje.toFixed(2).replace('.',',')) + "%";
                        }

                        tooltipDiv.transition()     
                            .duration(200)
                            .style('display', 'block')
                            .style('opacity','0.9')
                        tooltipDiv.html('<span style="font-weight: 900;">' + dato + '</span>')
                            .style("left", ""+ (event.pageX - 25) +"px")     
                            .style("top", ""+ (event.pageY - 35) +"px");
                    })
                    .on('touchend mouseout mouseleave', function(d) {
                        tooltipDiv.style('display','none').style('opacity','0');
                    });
        }

        unicoGrafico(newData, width);
    }
}

//Recuperación
function getTermometroEmpleo(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    let lineaEjeX = proyecto == 'dashboard' ? !slideWidth ? 'area-term_empleo-ejeX' : 'slider-term_empleo-ejeX' : 'term_empleo-ejeX';

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartTermometro(data, 'term_empleo', contenedorGrafico, lineaEjeX, proyecto) : 
            getChartTermometro(data, 'term_empleo', contenedorGrafico, lineaEjeX, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart7-1');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartTermometro(data, 'term_empleo', grafico, lineaEjeX, proyecto);
    }    
}

function getTermometroPIB(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    let lineaEjeX = proyecto == 'dashboard' ? !slideWidth ? 'area-term_pib-ejeX' : 'slider-term_pib-ejeX' : 'term_pib-ejeX';

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartTermometro(data, 'term_pib', contenedorGrafico, lineaEjeX, proyecto) : 
            getChartTermometro(data, 'term_pib', contenedorGrafico, lineaEjeX, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart7-2');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartTermometro(data, 'term_pib', grafico, lineaEjeX, proyecto);
    }
}

function getTermometroConsumo(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    let lineaEjeX = proyecto == 'dashboard' ? !slideWidth ? 'area-term_consumo-ejeX' : 'slider-term_consumo-ejeX' : 'term_consumo-ejeX';

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartTermometro(data, 'term_consumo', contenedorGrafico, lineaEjeX, proyecto) : 
            getChartTermometro(data, 'term_consumo', contenedorGrafico, lineaEjeX, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart7-3');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartTermometro(data, 'term_consumo', grafico, lineaEjeX, proyecto);
    }
}

function getTermometroRentaFamiliar(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    let lineaEjeX = proyecto == 'dashboard' ? !slideWidth ? 'area-term_renta-ejeX' : 'slider-term_renta-ejeX' : 'term_renta-ejeX';

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartTermometro(data, 'term_renta', contenedorGrafico, lineaEjeX, proyecto) : 
            getChartTermometro(data, 'term_renta', contenedorGrafico, lineaEjeX, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart7-4');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartTermometro(data, 'term_renta', grafico, lineaEjeX, proyecto);
    }
}

function getTermometroBeneficioEmpresarial(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    let lineaEjeX = proyecto == 'dashboard' ? !slideWidth ? 'area-term_empresarial-ejeX' : 'slider-term_empresarial-ejeX' : 'term_empresarial-ejeX';

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartTermometro(data, 'term_empresarial', contenedorGrafico, lineaEjeX, proyecto) : 
            getChartTermometro(data, 'term_empresarial', contenedorGrafico, lineaEjeX, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart7-5');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartTermometro(data, 'term_empresarial', grafico, lineaEjeX, proyecto);
    }
}

function getTermometroTurismo(data, div_id, proyecto, slideWidth = undefined, slideHeight = undefined){
    let contenedorGrafico = proyecto == 'dashboard' ? `#${div_id}` : document.getElementById(div_id);
    let lineaEjeX = proyecto == 'dashboard' ? !slideWidth ? 'area-term_turismo-ejeX' : 'slider-term_turismo-ejeX' : 'term_turismo-ejeX';

    if(proyecto == 'dashboard'){
        !slideWidth ? 
            getChartTermometro(data, 'term_turismo', contenedorGrafico, lineaEjeX, proyecto) : 
            getChartTermometro(data, 'term_turismo', contenedorGrafico, lineaEjeX, proyecto, slideWidth, slideHeight);
    } else {
        drawChartElements(contenedorGrafico, '#chart7-6');
        let grafico = contenedorGrafico.getElementsByClassName('panel-chart')[0];
        getChartTermometro(data, 'term_turismo', grafico, lineaEjeX, proyecto);
    }
}

//Función genérica para gráficos de Termómetro/Recuperación
function getChartTermometro(data, tipo, grafico, lineaEjeX, proyecto, slideWidth = undefined, slideHeight = undefined) {
    //Estructura fundamental del gráfico
    let margin = {top: 5, right: 5, bottom: 20, left: 45},
    width = (proyecto == 'dashboard' ? !slideWidth ? document.getElementById(grafico.substr(1)).parentElement.clientWidth : slideWidth : grafico.clientWidth) - margin.left - margin.right,
    height = (proyecto == 'dashboard' ? !slideHeight ? document.getElementById(grafico.substr(1)).parentElement.clientHeight : slideHeight : grafico.clientHeight) - margin.top - margin.bottom;

    //Inicialización del gráfico
    let chart = select(grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    //Eje X
    let x = scaleBand()
        .domain(data.map(function(d) { return d.tipo; }))
        .paddingInner(0.5)
        .range([0, width]);
    
    let xAxis = function(svg){
        svg.call(axisBottom(x))
        svg.call(function(g){g.select('.domain').attr('id', lineaEjeX).style('opacity','0')})
        svg.call(function(g){g.selectAll('.tick text').attr('font-size','13px')})
        svg.call(function(g){g.selectAll('.tick line').remove()});
    }        
    
    chart.append('g')
        .attr('transform', 'translate(0, ' + height +')')
        .call(xAxis);

    //Eje Y
    let y = scaleLinear()
        .domain([0,100])
        .range([height,0]);

    let yAxis = function(g){
        g.call(axisLeft(y).ticks(4))
        g.call(function(g){g.selectAll('.tick text').attr('class', function(d) {return d == 0 && 'tick-text-special'})})
        g.call(function(g){g.select('.domain').remove()})
        g.call(function(g){g.selectAll('.tick line')
            .attr('class', function(d) {return d == 0 && 'zero-base'})
            .attr("x1", '0%')
            .attr("x2", '' + (document.getElementById(lineaEjeX).getBoundingClientRect().width) + '')});
    }
    
    chart.append("g")
        .attr('transform', 'translate(0, 0)')
        .call(yAxis);

    //Line
    let line = d3line()
        .x(function(d){ return x(d.tipo) + x.bandwidth() / 2})
        .y(function(d){return y(d.porc_termometro)});
    
    chart.append("path")
        .datum(data)
        .attr('class', 'area-lines')
        .attr('transform', 'translate(0, 0)')
        .attr("d", line);

    //Area > set the gradient
    chart.append("linearGradient")				
        .attr("id", proyecto == 'dashboard' ? !slideWidth ? `area-${tipo}` : `slider-${tipo}` : `${tipo}`)			
        .attr("gradientUnits", "userSpaceOnUse")	
        .attr("x1", 0).attr("y1", y(min(data, function(d) {return d.porc_termometro})))			
        .attr("x2", 0).attr("y2", y(max(data, function(d) {return d.porc_termometro})))		
        .selectAll("stop")						
        .data([
            {offset: "0%", color: "rgba(255, 70, 70, 0)"},
            {offset: "80%", color: "rgba(255, 70, 70, 0.4)"},
            {offset: "100%", color: "rgba(255, 70, 70, 1)"}	
        ])					
        .enter()
        .append("stop")			
        .attr("offset", function(d) { return d.offset; })	
        .attr("stop-color", function(d) { return d.color; });

    let area = d3area()
        .x(function(d) { return x(d.tipo) + x.bandwidth() / 2 })	
        .y0(height)					
        .y1(function(d) { return y(d.porc_termometro); });

    chart.append('path')
        .datum(data)
        .attr('fill', proyecto == 'dashboard' ? !slideWidth ? `url(#area-${tipo})` : `url(#slider-${tipo})` : `url(#${tipo})`)
        .attr('d', area);
    
    //Quedan definir los círculos
    chart.selectAll('.circles')
        .data(data)
        .enter()
        .append('circle')
        .attr("r", 5)
        .attr("cx", function(d) { return x(d.tipo) + x.bandwidth() / 2 })
        .attr("cy", function(d) { return y(d.porc_termometro); })
        .style("fill", 'rgba(255, 70, 70, 1)')
        .style('opacity', '1')
        .on('touchstart mouseover', function(d) {
            tooltipDiv.transition().duration(200)
                .style('display', 'block')
                .style('opacity','0.9');

            let html = '';
            
            if(tipo == 'term_empleo') {
                if(d.tipo == 'Actual') {
                    html = '<span style="font-weight: 300;">' + d.fecha + '</span><span style="font-weight: 300;">Dato abs.: ' + getNumberWithCommas(d.total.replace('.','')) + ' trabajadores</span><span style="font-weight: 900;">Porc.: ' + d.porc_termometro.toFixed(1).replace('.',',') + '%</span>';
                } else {
                    html = '<span style="font-weight: 300;">' + d.fecha + '</span><span style="font-weight: 300;">Dato abs.: ' + getNumberWithCommas(d.total.replace('.','')) + ' trabajadores</span>';
                }
            } else if (tipo == 'term_pib') {
                if(d.tipo == 'Actual') {
                    html = '<span style="font-weight: 300;">' + d.fecha + '</span><span style="font-weight: 300;">Índice: ' + d.total + '</span><span style="font-weight: 900;">Porc.: ' + d.porc_termometro.toFixed(1).replace('.',',') + '%</span>';
                } else {
                    html = '<span style="font-weight: 300;">' + d.fecha + '</span><span style="font-weight: 300;">Índice: ' + d.total + '</span>';
                }
            } else if (tipo == 'term_consumo') {
                if(d.tipo == 'Actual') {
                    html = '<span style="font-weight: 300;">' + d.fecha + '</span><span style="font-weight: 300;">Índice: ' + d.total + '</span><span style="font-weight: 900;">Porc.: ' + d.porc_termometro.toFixed(1).replace('.',',') + '%</span>';
                } else {
                    html = '<span style="font-weight: 300;">' + d.fecha + '</span><span style="font-weight: 300;">Índice: ' + d.total + '</span>';
                }
            } else if (tipo == 'term_renta') {
                if(d.tipo == 'Actual') {
                    html = '<span style="font-weight: 300;">' + d.fecha + '</span><span style="font-weight: 300;">Dato abs.: ' + getNumberWithCommas(d.total.replace('.','')) + ' mill. €</span><span style="font-weight: 900;">Porc.: ' + d.porc_termometro.toFixed(1).replace('.',',') + '%</span>';
                } else {
                    html = '<span style="font-weight: 300;">' + d.fecha + '</span><span style="font-weight: 300;">Dato abs.: ' + getNumberWithCommas(d.total.replace('.','')) + ' mill. €</span>';
                }
            } else if (tipo == 'term_empresarial') {
                if(d.tipo == 'Actual') {
                    html = '<span style="font-weight: 300;">' + d.fecha + '</span><span style="font-weight: 300;">Dato abs.: ' + getNumberWithCommas(d.total.replace('.','')) + ' mill. €</span><span style="font-weight: 900;">Porc.: ' + d.porc_termometro.toFixed(1).replace('.',',') + '%</span>';
                } else {
                    html = '<span style="font-weight: 300;">' + d.fecha + '</span><span style="font-weight: 300;">Dato abs.: ' + getNumberWithCommas(d.total.replace('.','')) + ' mill. €</span>';
                }
            } else {
                if(d.tipo == 'Actual') {
                    html = '<span style="font-weight: 300;">' + d.fecha + '</span><span style="font-weight: 300;">Dato abs.: ' + getNumberWithCommas(d.total.replace('.','')) + ' turistas</span><span style="font-weight: 900;">Porc.: ' + d.porc_termometro.toFixed(1).replace('.',',') + '%</span>';
                } else {
                    html = '<span style="font-weight: 300;">' + d.fecha + '</span><span style="font-weight: 300;">Dato abs.: ' + getNumberWithCommas(d.total.replace('.','')) + ' turistas</span>';
                }
            }            

            tooltipDiv.html(html)
                .style("left", ""+ (event.pageX - 25) +"px")     
                .style("top", ""+ (event.pageY + 17.5) +"px");
        })
        .on('touchend mouseout mouseleave', function(d) {
            tooltipDiv.style('display','none').style('opacity','0');
        });

    let prueba = chart.selectAll('.current-data-text')
        .data(data)
        .enter()
        .append('svg')
        .attr('width', '48px')
        .attr('height', '24px')
        .attr("x", function(d) { 
            if(d.porc_termometro > 85) {
                return x(d.tipo) + (x.bandwidth() / 2) - 62;
            } else {
                return x(d.tipo) + (x.bandwidth() / 2) - 24;
            }
        })
        .attr("y", function(d) {
            if(d.porc_termometro > 85) {
                return y(d.porc_termometro) - 12; 
            } else {
                return y(d.porc_termometro) - 37.5; 
            }           
        })
        .attr('pointer-events','none');
    
    prueba.append('rect')        
        .attr('fill', 'rgba(255, 70, 70, 1)')
        .attr('opacity', function(d,i) {
            return i == 2 ? '1' : '0';
        })
        .attr("x", "0")
        .attr("y", "0")
        .attr('width', '48px')
        .attr('height', '24px');
    
    prueba.append('text')
        .attr('font-size', '13px')
        .attr('font-weight', '700')
        .attr('fill', '#000')
        .attr('opacity', function(d,i) {
            return i == 2 ? '1' : '0';
        })
        .attr("x", "50%")
        .attr("y", "50%")
        .attr('alignment-baseline', 'middle')
        .attr('text-anchor', 'middle')
        .text(function(d) {
            return d.porc_termometro.toFixed(1).replace('.',',') + '%';
        });
}

export { 
    getProduccionPibHistograma, 
    getProduccionInversionProductiva,
    getProduccionPibEurozona,
    // getProduccionEmpresasActivas,
    getProduccionProdIndustrial,
    getProduccionPibEmpleo,
    getExpectativasPrevisionEconomica,
    getExpectativasPrevisionParo,
    getExpectativasPrevisiones,
    getExpectativasConfianzaConsumidor,
    getExpectativasPMI,
    getCuentasDeficitHistorico,
    getCuentasDeudaPublica,
    // getCuentasRecaudacion,
    getCuentasDeficitCCAA,
    // getEmpleoAfiliacionProvincias,
    getEmpleoAfiliacionHistorico,
    getEmpleoTasaParoHistorica,
    getEmpleoNumeroParados,
    // getEmpleoCaidaEmpleo,
    getEmpleoTasaParoEuropa,
    getDemandaHistorica,
    // getDemandaSectorial,
    getDemandaCCAA,
    getDemandaEurozona,
    getSectoresCambioAfiliacion,
    // getSectoresLlegadaViajeros,
    // getSectoresValorAnadido,
    getTermometroEmpleo,
    getTermometroConsumo,
    getTermometroPIB,
    getTermometroRentaFamiliar,
    getTermometroBeneficioEmpresarial,
    getTermometroTurismo
};