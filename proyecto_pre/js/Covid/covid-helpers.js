let meses = ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'];

function csvToJson(csv){
    let lines=csv.split("\n");    
    let result = [];

    let headers = lines[0].split(",");
    let inicio = 1;

    for(let i = 0; i < headers.length; i++){
        if(headers[i].toLowerCase() == 'fecha / ccaa'){
            headers[i] = 'fecha';
        } else if (headers[i].toLowerCase() == 'españa'){
            headers[i] = 'muertes_diarias';
        } else {
            headers[i] = headers[i].trim();
        }
    }      

    for(let i=inicio; i<lines.length; i++){
        let obj = {};
        let currentline=lines[i].split(",");

        for(let j=0;j<headers.length;j++){
            if(headers[j] != ''){
                obj[headers[j]] = currentline[j];
            }            
        }
        result.push(obj);
    }

    return result;
}

function tsvToJson(tsv) {
    let lines=tsv.split("\n");    
    let result = [];

    let headers = lines[0].split("\t");
    headers = headers.map(item => item.replace(/\"/g, "").trim());
    let inicio = 1; 

    for(let i=inicio; i<lines.length; i++){
        let obj = {};
        let currentline=lines[i].split("\t");

        for(let j=0;j<headers.length;j++){
            if(headers[j] != ''){
                obj[headers[j]] = currentline[j].replace(/\./g,'').replace(',','.');
            }            
        }
        result.push(obj);
    }

    return result;
}

function getIconUrl(data) {
    var imageUrl;

    if ( data == 0 ) {
        imageUrl = 'https://www.ecestaticos.com/file/21a714ffe5f147cc6d7ae6502fd67dbd/1601022614-icon-igual.svg';
    } else {
        if ( data > 0 ) {
            imageUrl = 'https://www.ecestaticos.com/file/2d458a58400699dc7846f82387269083/1601022633-icon-subida.svg'
        } else {
            imageUrl = 'https://www.ecestaticos.com/file/4105d55e4dc58211314c61aa1196511f/1601022652-icon-bajada.svg'
        }
    }
    return imageUrl
}

function formatTime(date) {
    let split = date.split("/");
    return new Date(split[2], split[1] - 1, split[0]);
}

function removeAfterResize(grafico) {    
    //Eliminamos los hijos
    let borrado = document.getElementById(grafico);
    while(borrado.firstChild){
        borrado.removeChild(borrado.firstChild);
    }
}

export {
    meses,
    csvToJson,
    tsvToJson,
    getIconUrl,
    formatTime,
    removeAfterResize
}