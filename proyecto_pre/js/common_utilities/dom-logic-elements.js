//// URL

function getURLParam() {
    let response = 'covid';
    let parametros = window.location.search;

    if(parametros){
        const urlParametro = new URLSearchParams(parametros);
        let module = urlParametro.get('modulo');
        response = module == 'panel' ? 'panel' : 'covid'; 
    } 
    
    return response;
}

//// DOM

function drawCovidFirst() {
    document.getElementsByClassName('container')[0].classList.add('covid');
    widgetElement.classList.add('widget--covid');
    tabCovid.classList.add('tab--first', 'tab--active');
    tabPanel.classList.add('tab--last');
}

function drawPanelFirst() {
    document.getElementsByClassName('container')[0].classList.add('panel');
    widgetElement.classList.add('widget--panel');
    tabPanel.classList.add('tab--first', 'tab--active');
    tabCovid.classList.add('tab--last');
}

/// Helpers

function getNumberWithThousandsPoint(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function isMobile() {
    return window.innerWidth < 768
}

function whichViewportWidth() {
    let width = window.innerWidth;
    let result = '';

    if(width > 995){
        result = "desktop";
    } else if (width > 900 && width <= 995) {
        result = "mediumDesktop";
    } else if (width <= 767) {
        result = 'mobile';
    } else {
        result = 'tablet';
    }

    return result;
}

export {
    getURLParam,
    drawCovidFirst,
    drawPanelFirst,
    getNumberWithThousandsPoint,
    isMobile,
    whichViewportWidth
}