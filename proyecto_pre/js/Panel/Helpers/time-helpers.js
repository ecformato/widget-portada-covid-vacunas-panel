import { meses, mesesReducido } from './logic-helpers';

function formatTimeData(data){
    let result = [];
    for(let i = 0; i < data.length; i++){
        let item = data[i];
        let provincia = '';
        if (!Object.entries) {
            Object.entries = function( obj ){
              var ownProps = Object.keys( obj ),
                  i = ownProps.length,
                  resArray = new Array(i); // preallocate the Array
              while (i--)
                  resArray[i] = [ownProps[i], obj[ownProps[i]]];
          
              return resArray;
            };
        } else {
            Object.entries(item).forEach(function(item2, index) {
                if(index == 0){
                    provincia = item2[1];
                } else {
                    result.push({fecha: item2[0], provincia: provincia, dato: item2[1]});
                }            
            });
        }        
    }
    return result;
}

function formatTime(date) {
    let split = date.split("/");
    return new Date(split[1], split[0] - 1, 1);
}

function formatTimeV2(date, especifico) {
    let split = date.split("/");
    let number = 0;
    let year = split[1];
    if(especifico == 'cuentas_publicas'){
        for(let i = 0; i < mesesReducido.length; i++){
            if(split[0].substr(0,3).toLowerCase() == mesesReducido[i].toLowerCase()){
                number = i;
            }
        }
        year = year < 25 ? '20' + year : '19' + year; //Feo > Peor hay que adecuarse a lo dispuesto por Javier
    } else {
        for(let i = 0; i < meses.length; i++){
            if(split[0].toLowerCase() == meses[i].toLowerCase()){
                number = i;
            }
        }
    }
    
    return new Date(year, number, 1);
}

function formatMonthTime(date){
    let split = date.split("/");
    return new Date(split[1], 4 - 1, 1);
}

function formatDayTime(date){
    let split = date.split("/");
    return new Date(split[2], split[1] - 1, split[0]);
}

function formatYear(date) {
    let prueba = date.getFullYear();
    return prueba;
}

export { formatTimeData, formatTime, formatTimeV2, formatMonthTime, formatDayTime, formatYear };