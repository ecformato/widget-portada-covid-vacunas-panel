import { setChart } from './api-helpers';

function deliverChart(chart, index, panelData){
    let htmlChartId = '';
    //Preguntar primero si es desktop o mobile para indicar un ID u otro
    if(index == 0){
        htmlChartId = 'panel-chart-1';
    } else {
        htmlChartId = 'panel-chart-2';
    }

    //Para el pintado de la visualización necesitamos i) la caja concreta, ii) los datos del gráfico y iii) elegir la función correcta
    setChart(panelData, chart, htmlChartId);
}

export {
    deliverChart
}