import { bisectLeft, range as range_d3 } from 'd3-array';

/* Helpers */
let meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
let mesesReducido = ['ene', 'feb', 'mar', 'abr', 'may', 'jun', 'jul', 'ago', 'sep', 'oct', 'nov', 'dic'];

function isResponsive() {
    return window.innerWidth < 993;
}

function isMobile() {
    return window.innerWidth < 768;
}

function scaleBandPosition(mouse, x) {
    var xPos = mouse[0];
    var domain = x.domain(); 
    var range = x.range();
    var rangePoints = range_d3(range[0], range[1], x.step());
    let prueba = bisectLeft(rangePoints, xPos);
    return domain[prueba];
}

function getNumberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function calculateRegression(XaxisData, Yaxisdata) {
    let ReduceAddition = function(prev, cur) { return prev + cur; };
    
    // finding the mean of Xaxis and Yaxis data
    let xBar = XaxisData.reduce(ReduceAddition) * 1.0 / XaxisData.length;
    let yBar = Yaxisdata.reduce(ReduceAddition) * 1.0 / Yaxisdata.length;

    let SquareXX = XaxisData.map(function(d) { return Math.pow(d - xBar, 2); })
      .reduce(ReduceAddition);
    
    let ssYY = Yaxisdata.map(function(d) { return Math.pow(d - yBar, 2); })
      .reduce(ReduceAddition);
      
    let MeanDiffXY = XaxisData.map(function(d, i) { return (d - xBar) * (Yaxisdata[i] - yBar); })
      .reduce(ReduceAddition);
      
    let slope = MeanDiffXY / SquareXX;
    let intercept = yBar - (xBar * slope);
    
    // returning regression function
    return function(x){
      return x*slope+intercept;
    }
}

export { meses, mesesReducido, isResponsive, isMobile, scaleBandPosition, getNumberWithCommas, calculateRegression};