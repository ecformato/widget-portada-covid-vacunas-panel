import { panelDataCuentasPublicas, panelDataDemanda, panelDataEmpleo, panelDataExpectativas, panelDataProduccion, panelDataSectores, panelDataTermometro } from '../../common_utilities/api-constants';
import {
    getProduccionPibHistograma, 
    getProduccionInversionProductiva,
    getProduccionPibEurozona,
    // getProduccionEmpresasActivas,
    getProduccionProdIndustrial,
    getProduccionPibEmpleo,
    getExpectativasPrevisionEconomica,
    getExpectativasPrevisionParo,
    getExpectativasPrevisiones,
    getExpectativasConfianzaConsumidor,
    getExpectativasPMI,
    getCuentasDeficitHistorico,
    getCuentasDeudaPublica,
    // getCuentasRecaudacion,
    getCuentasDeficitCCAA,
    // getEmpleoAfiliacionProvincias,
    getEmpleoAfiliacionHistorico,
    getEmpleoTasaParoHistorica,
    getEmpleoNumeroParados,
    // getEmpleoCaidaEmpleo,
    getEmpleoTasaParoEuropa,
    getDemandaHistorica,
    // getDemandaSectorial,
    getDemandaCCAA,
    getDemandaEurozona,
    getSectoresCambioAfiliacion,
    getTermometroEmpleo,
    getTermometroConsumo,
    getTermometroPIB,
    getTermometroRentaFamiliar,
    getTermometroBeneficioEmpresarial,
    getTermometroTurismo,
    // getSectoresLlegadaViajeros,
    // getSectoresValorAnadido
} from '../Charts/charts';

// Recuperación de datos
function csvToJsonPanel(csv, specialData = undefined){
    let lines=csv.split("\n");    
    let result = [];

    let headers = specialData ? specialData == 'specificHomeCommon' ? lines[1].split("\t") : lines[5].split("\t") : lines[3].split("\t");
    let inicio = specialData ? specialData == 'specificHomeCommon' ? 2 : 6 : 4;

    for(let i = 0; i < headers.length; i++){
        headers[i] = headers[i].trim();
    }

    for(let i=inicio;i<lines.length;i++){ //Datos respectivos a headers, a partir de quinta línea
        let obj = {};
        let currentline=lines[i].split("\t");

        for(let j=0;j<headers.length;j++){
            if(headers[j] != ''){ //26/05 --> Queda una cabecera vacía. Lo dejamos así de momento
                obj[headers[j]] = currentline[j];
            }            
        }
        result.push(obj);
    }
    return result;
}

function apisToCall(chartsToShow){
    let setApis = new Set();

    for(let i = 0; i < chartsToShow.length; i++){
        switch(chartsToShow[i]['Area_Interes_Desarrollo']){
            case 'produccion':
                setApis.add('produccion');
                break;
            case 'expectativas':
                setApis.add('expectativas');
                break;
            case 'cuentas_publicas':
                setApis.add('cuentas_publicas');
                break;
            case 'empleo':
                setApis.add('empleo');
                break;
            case 'demanda':
                setApis.add('demanda');
                break;
            case 'sectores':
                setApis.add('sectores');
                break;
            case 'termometro':
                setApis.add('termometro');
                break;
            default:
                console.log("error");
                break;
        }
    }

    return setApis;
}

function retrievePromiseArray(specificApis){
    let promises = [];
    specificApis.forEach((item) => {
        switch(item){
            case 'produccion':
                promises.push(window.fetch(panelDataProduccion).then((response) => {return response.text()}));
                break;
            case 'expectativas':
                promises.push(window.fetch(panelDataExpectativas).then((response) => {return response.text()}));
                break;
            case 'cuentas_publicas':
                promises.push(window.fetch(panelDataCuentasPublicas).then((response) => {return response.text()}));
                break;
            case 'empleo':
                promises.push(window.fetch(panelDataEmpleo).then((response) => {return response.text()}));
                break;
            case 'demanda':
                promises.push(window.fetch(panelDataDemanda).then((response) => {return response.text()}));
                break;
            case 'sectores':
                promises.push(window.fetch(panelDataSectores).then((response) => {return response.text()}));
                break;
            case 'termometro':
                promises.push(window.fetch(panelDataTermometro).then((response) => {return response.text()}));
                break;
            default:
                console.log("error 2");
                break;
        }
    });
    return promises;
}

function setChart(panelData, chart, div_id){
    let auxiliarArray = [];
    let chart_id = chart['ID_Grafico'];

    switch(chart_id){
        case '#chart1-1':
            panelData.map((item) => {item.PIB && auxiliarArray.push({trimestre: item.Trimestre_PIB, trimestre_v2: item.Trimestre_PIB_Generico, pib_data: +item.PIB.replace(',','.')});});
            getProduccionPibHistograma(auxiliarArray, div_id, 'widget');
            break;
        case '#chart1-2':
            panelData.map((item) => {item.IP && auxiliarArray.push({trimestre: item.Trimestre_IP, trimestre_v2: item.Trimestre_IP_Generico, ip_data: +item.IP.replace(',','.')});})
            getProduccionInversionProductiva(auxiliarArray, div_id, 'widget');
            break;
        case '#chart1-3':
            panelData.map((item) => {(item['PAIS'] && item['Cuarto trimestre']) && auxiliarArray.push({pais: item['PAIS'], dato_trimestre: +item['Cuarto trimestre'].replace(',','.'), dato_trimestre_2: +item['Primer trimestre'].replace(',','.')});});
            getProduccionPibEurozona(auxiliarArray, div_id, 'widget');
            break;
        case '#chart1-5':            
            panelData.map((item) => {item.PI && auxiliarArray.push({id_ccaa: +item.ID_CCAA, ccaa: item.CCAA, pi_data: +item.PI.replace(',','.')});});
            getProduccionProdIndustrial(auxiliarArray, div_id, 'widget');
            break;
        case '#chart1-6':
            panelData.map((item) => {item.Trimestre_Correlacion && auxiliarArray.push({trimestre: item.Trimestre_Correlacion, pib_correlacion_data: +item.PIB_Correlacion.replace(',','.'), horas_efectivas_data: +item.Horas_efectivas.replace(',','.')});});
            getProduccionPibEmpleo(auxiliarArray, div_id, 'widget');
            break;
        case '#chart2-1':
            panelData.map((item) => {item.Mes_Evolucion_Economica && auxiliarArray.push({mes: item['Mes_Evolucion_Economica'], mes_v2: item['Mes_Evolucion_Eco_Generico'] ,situacion_economica: +item['General economic situation over the next 12 months'].replace(',','.')});});
            getExpectativasPrevisionEconomica(auxiliarArray, div_id, 'widget');
            break;
        case '#chart2-2':
            panelData.map((item) => {item.Mes_Evolucion_Paro && auxiliarArray.push({mes: item['Mes_Evolucion_Paro'], mes_v2: item['Mes_Evolucion_Paro_Generico'], situacion_empleo: +item['Unemployment expectations over the next 12 months'].replace(',','.')});});
            getExpectativasPrevisionParo(auxiliarArray, div_id, 'widget');
            break;
        case '#chart2-3':
            panelData.map((item) => { (item.ID_PAIS && item['III Trimestre 2021']) && auxiliarArray.push({id_pais: item.ID_PAIS, pais: item.PAIS, primer_trim: +item['III Trimestre 2021'].replace(',','.'), segundo_trim: +item['IV Trimestre 2021'].replace(',','.'), tercer_trim: +item['I Trimestre 2022'].replace(',','.')});}); //Cambiar
            getExpectativasPrevisiones(auxiliarArray, div_id, 'widget');
            break;
        case '#chart2-4':
            panelData.map((item) => {item.Mes_Confianza && auxiliarArray.push({mes: item.Mes_Confianza, mes_v2: item.Mes_Confianza_Generico, manufacturas: +item['Manufacturas'].replace(',','.'), servicios: +item['Servicios'].replace(',','.'), total_economia: +item['Total economía'].replace(',','.')});});
            getExpectativasConfianzaConsumidor(auxiliarArray, div_id, 'widget');
            break;
        case '#chart2-5':
            panelData.map((item) => {item.PAIS_CONFIANZA && auxiliarArray.push({pais: item.PAIS_CONFIANZA, confianza: +item.Porcentaje_Confianza.replace(',','.')});});
            getExpectativasPMI(auxiliarArray, div_id, 'widget');
            break;
        case '#chart3-1':
            panelData.map((item) => {item.Mes_Deficit_Generico && auxiliarArray.push({mes: item.Mes_Deficit_Generico, deficit: +item.Deficit_Dato.replace(',','.')});});
            getCuentasDeficitHistorico(auxiliarArray, div_id, 'widget');
            break;
        case '#chart3-2':
            panelData.map((item) => {item.Trimestre_Deuda_Generico && auxiliarArray.push({trimestre: item.Trimestre_Deuda_Generico, porcentaje_deuda: +item.Porcentaje_Deuda.replace(',','.')});});
            getCuentasDeudaPublica(auxiliarArray, div_id, 'widget');
            break;
        case '#chart3-4':
            panelData.map((item) => { (item.ID_CCAA && item.Deficit_Acumulado) && auxiliarArray.push({id_ccaa: item.ID_CCAA, ccaa: item.CCAA, deficit_acumulado: +item.Deficit_Acumulado.replace(',','.')});});
            getCuentasDeficitCCAA(auxiliarArray, div_id, 'widget');
            break;
        case '#chart4-2':
            panelData.map((item) => {item.Periodo_Afiliacion && auxiliarArray.push({fecha: item.Periodo_Afiliacion, afiliados: +item.Afiliados});});
            getEmpleoAfiliacionHistorico(auxiliarArray, div_id, 'widget');
            break;
        case '#chart4-3':
            panelData.map((item) => {item.Trimestre_Paro_Generico && auxiliarArray.push({trimestre: item.Trimestre_Paro, trimestre_v2: item.Trimestre_Paro_Generico, tasa_paro: +item.Tasa_Paro.replace(',','.')});});
            getEmpleoTasaParoHistorica(auxiliarArray, div_id, 'widget');
            break;
        case '#chart4-4':
            panelData.map((item) => {item.Mes_Paro && auxiliarArray.push({mes: item.Mes_Paro, mes_v2: item.Mes_Paro_Generico, paro_registrado: +item.Paro_Registrado});});
            getEmpleoNumeroParados(auxiliarArray, div_id, 'widget');
            break;
        case '#chart4-6':
            panelData.map((item) => {item.Pais_Paro && auxiliarArray.push({pais: item.Pais_Paro, datos_paro: +item.Dato_Paro.replace(',','.')});});
            getEmpleoTasaParoEuropa(auxiliarArray, div_id, 'widget');
            break;
        case '#chart5-1':
            panelData.map((item) => {item.Mes_Historico && auxiliarArray.push({mes: item.Mes_Historico, mes_v2: item.Mes_Historico_Generico, indice_precios_corrientes: +item['Índice'].replace(',','.')});});
            getDemandaHistorica(auxiliarArray, div_id, 'widget');
            break;
        case '#chart5-3':
            panelData.map((item) => { (item.ID_CCAA && item['Variación mensual']) && auxiliarArray.push({id_ccaa: item.ID_CCAA, ccaa: item.CCAA, variacion_mensual: +item['Variación mensual'].replace(',','.'), variacion_anual: +item['Variación anual'].replace(',','.')});});
            getDemandaCCAA(auxiliarArray, div_id, 'widget');
            break;
        case '#chart5-4':
            panelData.map((item) => { (item.ID_PAIS && item['Consumo']) && auxiliarArray.push({id_pais: item.ID_PAIS, pais: item.PAIS, consumo: +item['Consumo'].replace(',','.')});});
            getDemandaEurozona(auxiliarArray, div_id, 'widget');
            break;
        case '#chart6-1':
            panelData.map((item) => {item.Sector_Afiliacion && auxiliarArray.push({sector: item.Sector_Afiliacion, sector_mostrar: item.Sector_Af_Mostrar, data_porcentaje: +item.Porcentaje_Afiliacion.replace(',','.'), data_absoluto: +item.Absoluto_Afiliacion.replace(',','.')});});
            getSectoresCambioAfiliacion(auxiliarArray, div_id, 'widget');
            break;
        case '#chart7-1':
            panelData.map((item) => { item.Tipo_Empleo && auxiliarArray.push({tipo: item.Tipo_Empleo, fecha: item.Fecha_Empleo, total: item.Total_Empleo, porc_termometro: +item.Termometro_Empleo.replace(',','.')})});
            getTermometroEmpleo(auxiliarArray, div_id, 'widget');
            break;
        case '#chart7-2':
            panelData.map((item) => {item.Tipo_PIB && auxiliarArray.push({tipo: item.Tipo_PIB, fecha: item.Fecha_PIB, total: item.Total_PIB, porc_termometro: +item.Termometro_PIB.replace(',','.')})});
            getTermometroPIB(auxiliarArray, div_id, 'widget');
            break;
        case '#chart7-3':
            panelData.map((item) => {item.Tipo_Consumo && auxiliarArray.push({tipo: item.Tipo_Consumo, fecha: item.Fecha_Consumo, total: item.Total_Consumo, porc_termometro: +item.Termometro_Consumo.replace(',','.')})});
            getTermometroConsumo(auxiliarArray, div_id, 'widget');
            break;
        case '#chart7-4':
            panelData.map((item) => {item.Tipo_RentaFamiliar && auxiliarArray.push({tipo: item.Tipo_RentaFamiliar, fecha: item.Fecha_RentaFamiliar, total: item.Total_RentaFamiliar, porc_termometro: +item.Termometro_RentaFamiliar.replace(',','.')})});
            getTermometroRentaFamiliar(auxiliarArray, div_id, 'widget');
            break;
        case '#chart7-5':
            panelData.map((item) => {item.Tipo_BeneficioEmpresarial && auxiliarArray.push({tipo: item.Tipo_BeneficioEmpresarial, fecha: item.Fecha_BeneficioEmpresarial, total: item.Total_BeneficioEmpresarial, porc_termometro: +item.Termometro_BeneficioEmpresarial.replace(',','.')})});
            getTermometroBeneficioEmpresarial(auxiliarArray, div_id, 'widget');
            break;
        case '#chart7-6':
            panelData.map((item) => {item.Tipo_Turismo && auxiliarArray.push({tipo: item.Tipo_Turismo, fecha: item.Fecha_Turismo, total: item.Total_Turismo, porc_termometro: +item.Termometro_Turismo.replace(',','.')})});
            getTermometroTurismo(auxiliarArray, div_id, 'widget');
            break;
        default:
            console.log("error 3");
            break;
    }
}

export {
    csvToJsonPanel,
    apisToCall,
    retrievePromiseArray,
    setChart
}