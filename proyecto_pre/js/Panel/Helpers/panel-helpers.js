import { drawDataHome, setChart } from '../Helpers/draw-helpers';

const dataHomePibProduccion = [];
const dataHomePibExpectativas = [];
const dataHomeDeficitCuentasPublicas = [];

function initDataHome(data){
    for(let i = 0; i < data.length; i++){
        data[i].Trimestre_PIB_Generico != '' && dataHomePibProduccion.push({trimestre: data[i].Trimestre_PIB, trimestre_v2: data[i].Trimestre_PIB_Generico, pib_data: +data[i].PIB.replace(',','.')});
        data[i].ID_PAIS != '' && dataHomePibExpectativas.push({id_pais: data[i].ID_PAIS, pais: data[i].PAIS, trimestre_mostrar: +data[i]['III Trimestre'].replace(',','.'), trimestre_siguiente: +data[i]['IV Trimestre'].replace(',','.'), trimestre_siguiente_2: +data[i]['I Trimestre'].replace(',','.')});
        data[i].Mes_Deficit_Generico != '' && dataHomeDeficitCuentasPublicas.push({mes: data[i].Mes_Deficit, mes_v2: data[i].Mes_Deficit_Generico, deficit: +data[i].Deficit_Dato.replace(',','.')});
    }
    
    //Desde aquí podemos pintar los tres elementos en el DOM (pibProduccion, pibExpectativas y deficitCuentasPublicas)
    drawDataHome(dataHomePibProduccion, dataHomePibExpectativas, dataHomeDeficitCuentasPublicas);
}

function deliverChart(chart, index, panelData){
    let htmlChartId = '';
    //Preguntar primero si es desktop o mobile para indicar un ID u otro
    if(index == 0){
        //htmlChartId = mobile ? 'panel-chart-1--mobile' : 'panel-chart-1';
        htmlChartId = 'panel-chart-1';
    } else {
        //htmlChartId = mobile ? 'panel-chart-2--mobile' : 'panel-chart-2';
        htmlChartId = 'panel-chart-2';
    }

    //Para el pintado de la visualización necesitamos i) la caja concreta, ii) los datos del gráfico y iii) elegir la función correcta
    setChart(panelData, chart, htmlChartId);
}

export {
    initDataHome,
    deliverChart
}