////Bloque 'Rebrotes'
const covidDataMuertes = "https://api.elconfidencial.com/service/resource/widget-rebrotes-v2-evolucion-muertes/";
const covidDataContagios = "https://api.elconfidencial.com/service/resource/coronavirus-widget-rebrotes-spain/";
const covidDataVacunas = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vTpRjtEFvRA-MATFdLvFcR_feCorkg84shetw4nYcsCB6Sa8mvR2-IK_blhIenJq5Tx_rudWREECLLi/pub?gid=1953409114&single=true&output=tsv';

////Bloque 'Panel'
const panelCommonElements = 'https://api.elconfidencial.com/service/resource/dashboard-economia-elementos-comunes/';
const panelDataProduccion = 'https://api.elconfidencial.com/service/resource/dashboard-economia-produccion/';
const panelDataExpectativas = 'https://api.elconfidencial.com/service/resource/dashboard-economia-expectativas/';
const panelDataCuentasPublicas = 'https://api.elconfidencial.com/service/resource/dashboard-economia-cuentas-publicas/';
const panelDataSectores = 'https://api.elconfidencial.com/service/resource/dashboard-economia-sectores/';
const panelDataEmpleo = 'https://api.elconfidencial.com/service/resource/dashboard-economia-empleo/';
const panelDataDemanda = 'https://api.elconfidencial.com/service/resource/dashboard-economia-demanda/';
const panelDataTermometro = 'https://api.elconfidencial.com/service/resource/dashboard-economia-termometro/';

export {
    covidDataMuertes,
    covidDataContagios,
    covidDataVacunas,
    panelCommonElements,
    panelDataProduccion,
    panelDataExpectativas,
    panelDataCuentasPublicas,
    panelDataSectores,
    panelDataEmpleo,
    panelDataDemanda,
    panelDataTermometro
}