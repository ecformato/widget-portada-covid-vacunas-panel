//Relacionado con llamadas 'fetch' (para uso en IE11)
import 'whatwg-fetch';
import { panelCommonElements } from '../common_utilities/api-constants';
import { initDataHome } from './Helpers/api-helpers';
import { deliverChart } from './Helpers/deliver-helpers';

import {
    apisToCall,
    csvToJsonPanel,
    retrievePromiseArray
} from './Helpers/api-helpers';

let dataElementosComunes = [];

export function createPanelCharts(){    
    window.fetch(panelCommonElements)
        .then((response) => {return response.text()})
        .then((data) => {
            //Selección de dos gráficos a mostrar en widget
            let everyChart = csvToJsonPanel(data, 'specificHomeCommon');
            let chartsToShow = everyChart.filter(function(item){
                if(item.Mostrando_Widget.trim() != ""){
                    return item;
                }
            });
            function comparativa(a,b){
                const datoA = isNaN(a.Mostrando_Widget) ? 100 : parseInt(a.Mostrando_Widget); //Si es una x o cualquier otra letra, que pase a ser el último elemento
                const datoB = isNaN(b.Mostrando_Widget) ? 100 : parseInt(b.Mostrando_Widget); //Si es una x o cualquier otra letra, que pase a ser el último elemento
        
                let comparacion = 0;
                if (datoA >= datoB) {
                    comparacion = 1;
                } else if (datoA < datoB) {
                    comparacion = -1;
                }
                return comparacion;
            }
            chartsToShow = chartsToShow.sort(comparativa);

            //También habría que pensar en una solución por si hubiese una o ninguna cruz marcada en la hoja de cálculo
            //Si no hay ninguna cruz marcada, indicamos dos gráficos por defecto
            if(chartsToShow.length == 0){
                let charts = everyChart.filter(function(item){
                    if(item.ID_Grafico == '#chart2-3' || item.ID_Grafico == '#chart5-3'){return item;}
                });
                let chart1 = charts[0];
                let chart2 = charts[1];
                chartsToShow.push(chart1);
                chartsToShow.push(chart2);
            } else if (chartsToShow.length == 1){
                let charts = [];
                if(chartsToShow[0].ID_Grafico == '#chart2-3'){
                    charts = everyChart.filter(function(item){
                        if(item.ID_Grafico == '#chart5-3'){return item;}
                    });
                } else {
                    charts = everyChart.filter(function(item){
                        if(item.ID_Grafico == '#chart2-3'){return item;}
                    });
                }
                
                chart1 = charts[0];
                chartsToShow.push(chart1);
            }

            //En la hoja de cálculo no debería de haber más de dos cruces. Si hay más, se corta con las dos primeras
            chartsToShow = chartsToShow.slice(0,2);

            //Doble visualización
            //Propiedades de los dos gráficos seleccionados
            dataElementosComunes = chartsToShow.slice();
            
            //APIs de los dos gráficos seleccionados
            let specificApis = apisToCall(chartsToShow);
            let apisPromises = retrievePromiseArray(specificApis);

            Promise.all(apisPromises)
                .then((promises) => {
                    let panelData = [];
                    for(let i = 0; i < promises.length; i++){
                        if(promises[i].indexOf('Termometro_Empleo') != -1) {
                            panelData = panelData.concat(csvToJsonPanel(promises[i], 'specificTermometro'));
                        } else {
                            panelData = panelData.concat(csvToJsonPanel(promises[i]));
                        }
                        
                    }                    

                    //Dibujo de los dos gráficos seleccionados
                    for(let i = 0; i < dataElementosComunes.length; i++){
                        deliverChart(dataElementosComunes[i], i, panelData);
                    }                    
                });
            });
}

export {
    dataElementosComunes
}