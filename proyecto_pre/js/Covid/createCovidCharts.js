//Relacionado con llamadas 'fetch' (para uso en IE11)
import 'whatwg-fetch';

//Uso de librería d3
import { select, event } from 'd3-selection';
import { nest } from 'd3-collection';
import { scaleBand, scaleTime, scaleLinear } from 'd3-scale';
import { max, extent, range } from 'd3-array';
import { axisBottom, axisLeft } from 'd3-axis';
import { line as d3line, curveCatmullRom } from 'd3-shape';
import { timeDay } from 'd3-time';

//Variables y funciones relativas a Covid-19
import { covidDataMuertes, covidDataContagios, covidDataVacunas } from '../common_utilities/api-constants';
import { meses, csvToJson, tsvToJson, formatTime, removeAfterResize } from './covid-helpers';
import { getNumberWithThousandsPoint, isMobile } from '../common_utilities/dom-logic-elements';
import { tooltipDiv } from '../common_utilities/tooltip';

let dataMuertes = [], dataCasos = [], dataVacunas = [];

export function createCovidCharts(){
    Promise.all([
        window.fetch(covidDataContagios).then((response) => {return response.text()}),
        window.fetch(covidDataMuertes).then((response) => {return response.text()}),
        window.fetch(covidDataVacunas).then((response) => {return response.text()})
    ]).then(function(responses){
        //Formateo de elementos (excepto del mapa)
        dataCasos = csvToJson(responses[0]);
        dataMuertes = csvToJson(responses[1]);
        dataVacunas = tsvToJson(responses[2]);

        //Pintado de elementos
        covidDoubleChart(dataCasos, dataMuertes);
        vacunasDoubleChart(dataVacunas);
    }).catch(function(error){console.log(error);})    
}

///////
// Doble gráfico de vacunación
//////
function vacunasDoubleChart(dataVacunas) {
    //Variables internas al bloque
    let selector = document.getElementById('vacunas-provincias');
    let histogram = document.getElementById('vaccine-histogram');

    let opcionDefecto = '20';
    let dataProvincias = [], filteredData = [], maxData = 0;

    let margin, width, height, svg, x, xAxis, y, yAxis, line;

    //Agrupamos por provincia (y a nivel nacional)
	let dataProvinciasAux = nest()
        .key(function(d) {return d.Cod;})
        .entries(dataVacunas);

    dataProvinciasAux = dataProvinciasAux.sort(comparativa);

    dataProvincias = [...dataProvinciasAux];    

    function initCharts(data, opcion) {
        filteredData = filteringData(data, opcion);
        maxData = filteringMaxData(filteredData);

        let t = select(histogram).transition().duration(2500);

        //Barra inferior
        let unaDosis = filteredData.values[filteredData.values.length - 1]['%vacunados'];
        document.getElementById('vaccine-pauta').style.width = unaDosis + '%';
        document.getElementById('poblacion-vacunada').textContent = unaDosis.replace('.',',');

        //Creación del gráfico
        margin = {top: 5, right: 7.5, bottom: 20, left: 55},
        width = document.getElementById('vaccine-histogram').clientWidth - margin.left - margin.right,
        height = document.getElementById('vaccine-histogram').clientHeight - margin.top - margin.bottom;

        svg = select(histogram)
            .append("svg")
            .attr('id', 'histograma')
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //Eje X >> Mejorar formato de fecha
        x = scaleBand()
            .domain(filteredData.values.map(function(d) { return d.Publicación }))
            .range([0, width])
            .padding(0.1);

        //Mostrar eje X
        xAxis = function(svg){
            svg.call(axisBottom(x).tickFormat(function(d){
                let split = d.split("/");
                return parseInt(split[0]) + " " + meses[parseInt(split[1] - 1)]; 
            }))
            svg.call(function(g){g.selectAll('.tick').attr('opacity', function(d, i){return i == 0 || i == Math.round(filteredData.values.length / 2) || i == filteredData.values.length - 1 ? '1' : '0' })})
            svg.call(function(g){g.selectAll('.tick').attr('text-anchor', function(d, i){return i == 0 ? 'start' : i == Math.round(filteredData.values.length / 2) ? 'middle' : 'end'})})
            svg.call(function(g){g.selectAll('.tick line').remove()})
            svg.call(function(g){g.select('.domain').attr('id', 'eje-x-vacunas').attr('opacity','0')})
            svg.call(function(g){g.selectAll('.tick text').attr('class', 'axis')});
        }

        svg.append('g')
            .attr('class', 'x-axis')
            .attr('transform', 'translate(0, ' + (height) +')')
            .call(xAxis);

        //Eje Y
        y = scaleLinear()
            .domain([0,maxData])
            .range([height,0]);

        yAxis = function(svg){
            svg.call(axisLeft(y).ticks(5).tickFormat((d) => { return getNumberWithThousandsPoint(d) }))
            svg.call(function(g){g.selectAll('.tick line')            
                //.attr("stroke", "#000")
                //.attr("stroke-dasharray", function(d) {return d == 0 ? '0' : '2'})
                //.attr("stroke-opacity", function(d) {return d == 0 ? '1' : '.5'})
                .attr("stroke", function(d) {return d == 0 ? '#000' : '#dadada'})
                .attr("stroke-dasharray", function(d) {return d == 0 ? '0' : '1'})
                //.attr("x1", '0%')
                .attr("x1", '0.5%')
                .attr("x2", '' + (document.getElementById('eje-x-vacunas').getBoundingClientRect().width) + '')
            })
            svg.call(function(g){g.selectAll('.tick text').attr('class', 'axis')})
        }

        svg.append('g')
            .attr('class', 'y-axis')
            .call(yAxis);

        //Barras
        svg.selectAll('.barras')
            .data(filteredData.values)
            .enter()
            .append('rect')
            .attr('class', 'barras-pcr')
            .attr("x", function(d) { return x(d.Publicación); })
            .attr("y", function(d) { return y(d.Nuevas_diarias); })
            .attr("width", x.bandwidth())
            .attr("height", function(d) { return height - y(d.Nuevas_diarias); })
            .attr('fill', '#e0e4e9')
            .on('touchmove mouseover', function(d,i){
                //Tooltip
                let fecha = d.Publicación.split("/");
                fecha = fecha[0] + "/" + fecha[1];			
                
                let valorMedia = filteredData.values[i]['Media_7 dias'];
                
                tooltipDiv.transition()     
                    .duration(200)
                    .style('background-color', '#f7f7f7')
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 300;">Fecha: ' + fecha + '</span><span style="font-weight: 900;">Vacunas diarias: ' + getNumberWithThousandsPoint(d.Nuevas_diarias) + '</span><span style="font-weight: 900;">Media: ' + getNumberWithThousandsPoint(valorMedia.toString().replace('.',',')) + '</span>')
                    .style("left", ""+ (event.pageX - (window.innerWidth < 939 ? 90 : 75)) +"px")     
                    .style("top", ""+ (event.pageY - 75) +"px");
            })
            .on('touchend mouseout mouseleave', function(d){
                tooltipDiv.style('display','none').style('opacity','0');
            });

        line = d3line()
            .x(function(d) { return x(d.Publicación) + x.bandwidth() / 2; })
            .y(function(d) { return y(d['Media_7 dias']); })
            .curve(curveCatmullRom.alpha(0.5));
    
        svg.append('path')
            .attr('class', 'linea-media')
            .datum(filteredData.values)
            .attr('fill', 'none')
            //.attr('stroke-width', '2.5px')
            .attr('stroke-width', '1.5px')
            .attr('stroke', '#414257')
            .attr('d', line);

        //Círculos ocultos (para mostrar tooltip)
        svg.selectAll('.circles')
            .data(filteredData.values)
            .enter()
            .append('circle')
            .attr('class', 'circulos-media')
            .attr("r", 6)
            .attr("cx", function(d) { return x(d.Publicación) + x.bandwidth() / 2; })
            .attr("cy", function(d) { return y(d['Media_7 dias']); })
            .style("fill", '#414257')
            .style('opacity', '0')
            .on('touchmove mouseover', function(d,i){
                //Tooltip
                let fecha = d.Publicación.split("/");
                fecha = fecha[0] + "/" + fecha[1];

                let valorDiario = filteredData.values[i].Nuevas_diarias;
                
                tooltipDiv.transition()     
                    .duration(200)
                    .style('background-color', '#f7f7f7')
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 300;">Fecha: ' + fecha + '</span><span style="font-weight: 900;">Vacunas diarias: ' + getNumberWithThousandsPoint(valorDiario) + '</span><span style="font-weight: 900;">Media: ' + getNumberWithThousandsPoint(d['Media_7 dias'].toString().replace('.',',')) + '</span>')
                    .style("left", ""+ (event.pageX - (window.innerWidth < 939 ? 90 : 60)) +"px")      
                    .style("top", ""+ (event.pageY - 75) +"px");
            })
            .on('touchend mouseout mouseleave', function(d){
                tooltipDiv.style('display','none').style('opacity','0');
            });
        
    }   

    function updateCharts(data, opcion) {
        filteredData = filteringData(data, opcion);
        maxData = filteringMaxData(filteredData);

        //Barra inferior
        let unaDosis = filteredData.values[filteredData.values.length - 1]['%vacunados'];	
        console.log(unaDosis);
        document.getElementById('vaccine-pauta').style.width = `${+unaDosis}%`;
        document.getElementById('poblacion-vacunada').textContent = unaDosis.replace('.',',');

        let t = select(histogram).transition().duration(2500);

        //Cambiamos dominio de eje Y
        y.domain([0,maxData]);
        t.select('.y-axis').call(yAxis);

        //Cambiamos datos para barras, línea y círculos
        svg.selectAll('.barras-pcr')
            .data(filteredData.values)
            .transition(t)
            .attr("x", function(d) { return x(d.Publicación); })
            .attr("y", function(d) { return y(+d.Nuevas_diarias); })
            .attr("height", function(d) { return height - y(+d.Nuevas_diarias); });

        svg.selectAll('.linea-media')
            .datum(filteredData.values)
            .transition(t)
            .attr('d', line);

        svg.selectAll('.circulos-media')
            .data(filteredData.values)
            .transition(t)
            .attr("cy", function(d) { return y(+d['Media_7 dias']); });
    }

    selector.addEventListener('change', (e) => {
        opcionDefecto = selector.value;
        updateCharts(dataProvincias, opcionDefecto);
    });

    //Primer proceso
    initCharts(dataProvincias, opcionDefecto);
    document.getElementById('lastUpdate').textContent = dataProvincias[0].values[dataProvincias[0].values.length - 1].Publicación;
}

///////
// Doble gráfico de casos Covid
///////
function covidDoubleChart(dataCasos, dataMuertes){
    //Filtrado de casos
    let newDataCasos = dataCasos.slice(19);
    let newDataMuertes = dataMuertes.slice(30);

    newDataCasos = newDataCasos.filter(function(d){
        if(d.fecha != "" && !isNaN(d.dif_contagios_media)){
            return d;
        }});

    newDataMuertes = newDataMuertes.filter(function(d){
        if(d.fecha != "" && !isNaN(d.muertes_media)){
            return d;
        }});

    //Dibujo del doble gráfico
    //let grafico = isMobile() == true ? 'evolucion_casos--mobile-chart' : 'evolucion_casos--desktop';
    let grafico = 'evolucion_casos--desktop';
    removeAfterResize(grafico);

    let margin = {top: 5, right: 12.5, bottom: 20, left: 45},
    width = document.getElementById(grafico).clientWidth - margin.left - margin.right,
    height = document.getElementById(grafico).clientHeight - margin.top - margin.bottom;

    let svg = select('#'+grafico)
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    /**
     * 
     * Primer apartado 
     * 
    **/ 
    let maxCasos = max(newDataCasos, (d) => { return +d.dif_contagios_media});
        
    //Eje X
    let x1 = scaleBand()
        .range([0,width])
        .domain(range(newDataCasos.length))
        .padding(.5)
        .align(1)

    let x2 = scaleTime()
        .domain(extent(newDataCasos, function(d) {return formatTime(d.fecha)}))
        .range([0, width]);

    let xAxis = function(svg){
        svg.call(axisBottom(x2).ticks(timeDay).tickFormat(function(d){let day = d.getDate(), month = d.getMonth(); return parseInt(day) + " " + meses[parseInt(month)];}))
        svg.call(function(g){g.selectAll('.tick').attr('opacity', function(d, i){return i == 0 || i == Math.round(newDataCasos.length / 2) || i == newDataCasos.length - 1 ? '1' : '0' })}) //Cambiar en mobile
        svg.call(function(g){g.selectAll('.tick').attr('text-anchor', function(d, i){return i == 0 ? 'start' : i == Math.round(newDataCasos.length / 2) ? 'middle' : 'end'})})
        svg.call(function(g){g.selectAll('.tick line').remove()})
        svg.call(function(g){g.select('.domain').attr('id', 'eje-x').attr('opacity','0')})
        svg.call(function(g){g.selectAll('.tick text').attr('class','axis')});
    }

    svg.append('g')
        .attr('transform', 'translate(0, ' + (height) +')')
        .call(xAxis);

    //Eje Y
    let y1 = scaleLinear()
        .domain([0,maxCasos])
        .range([height / 2, 0]);    

    let yAxis = function(svg){
        svg.call(axisLeft(y1).ticks(5).tickFormat((d) => { return getNumberWithThousandsPoint(d) }))
        svg.call(function(g){g.selectAll('.tick').attr('opacity', function(d) {return d == 0 ? '0' : '1'})})
        svg.call(function(g){g.selectAll('.tick line')            
            //.attr("stroke", "#000")
            //.attr("stroke-dasharray", function(d) {return d == 0 ? '0' : '2'})
            //.attr("stroke-opacity", function(d) {return d == 0 ? '0' : '.5'})
            .attr("stroke", function(d) {return d == 0 ? '#000' : '#dadada'})
            .attr("stroke-dasharray", function(d) {return d == 0 ? '0' : '1'})
            //.attr("x1", '0%')
            .attr("x1", '0.5%')
            .attr("x2", '' + (document.getElementById('eje-x').getBoundingClientRect().width) + '')
        })
        svg.call(function(g){g.selectAll('.tick text')
            //.attr('class', function(d) {return d == 0 ? '' : 'axis blue'})
            .attr('class', function(d) {return d == 0 ? '' : 'axis'})
        })
    }

    svg.append('g')
        .call(yAxis);

    //Barras
    svg.selectAll('.barras')
        .data(newDataCasos)
        .enter()
        .append('rect')
        .attr("x", function(d,i) { return x1(i); })
        .attr("y", function(d) { return y1(+d.dif_contagios_media); })
        .attr("width", x1.bandwidth())
        .attr("height", function(d) { return (height / 2) - y1(+d.dif_contagios_media); })
        .attr('fill', '#414257')
        .on('touchmove mouseover', function(d,i){
            let fecha = d.fecha.split("/");
            fecha = fecha[0] + "/" + fecha[1];
            
            tooltipDiv.transition()     
                .duration(200)
                .style('background-color', '#f7f7f7')
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipDiv.html('<span style="font-weight: 300;">Fecha: ' + fecha + '</span><span style="font-weight: 900;">Media contagios: ' + getNumberWithThousandsPoint(d.dif_contagios_media) + '</span>')
                .style("left", ""+ (event.pageX - (window.innerWidth < 525 ? 80 : 70)) +"px")     
                .style("top", ""+ (event.pageY - 60) +"px");
        })
        .on('touchend mouseout mouseleave', function(d){
            tooltipDiv.style('display','none').style('opacity','0');
        });

    /**
     * 
     * Segundo apartado 
     * 
    **/
    let maxMuertes = max(newDataMuertes, (d) => { return +d.muertes_media});

    //Eje X
    let x3 = scaleBand()
        .range([0,width])
        .domain(range(newDataMuertes.length))
        .padding(.5)
        .align(1);

    let x4 = scaleTime()
        .domain(extent(newDataMuertes, function(d) {return formatTime(d.fecha)}))
        .range([0, width]);

    let xAxis2 = function(svg){
        svg.call(axisBottom(x4))
        svg.call(function(g){g.selectAll('.tick').remove()})
        svg.call(function(g){g.selectAll('.tick line').remove()})
        svg.call(function(g){g.select('.domain').attr('id', 'eje-x-2').attr('opacity','0')})
        svg.call(function(g){g.selectAll('.tick text').remove()});
    }

    svg.append('g')
        .attr('transform', 'translate(0, ' + (height) +')')
        .call(xAxis2);

    //Eje Y
    let y2 = scaleLinear()
        .domain([maxMuertes,0])
        .range([height, height / 2]);    

    let yAxis2 = function(svg){
        svg.call(axisLeft(y2).ticks(5).tickFormat((d) => { return getNumberWithThousandsPoint(d) }))
        svg.call(function(g){g.selectAll('.tick line')            
            //.attr("stroke", "#000")
            //.attr("stroke-dasharray", function(d) {return d == 0 ? '0' : '2'})
            //.attr("stroke-opacity", function(d) {return d == 0 ? '1' : '.5'})
            .attr("stroke", function(d) {return d == 0 ? '#000' : '#dadada'})
            .attr("stroke-dasharray", function(d) {return d == 0 ? '0' : '1'})
            //.attr("x1", '0%')
            .attr("x1", '0.5%')
            .attr("x2", '' + (document.getElementById('eje-x-2').getBoundingClientRect().width) + '')
        })
        //svg.call(function(g){g.selectAll('.tick text').attr('class', function(d){ return d == 0 ? 'axis' : 'axis red' })})
        svg.call(function(g){g.selectAll('.tick text').attr('class', 'axis')})
    }

    svg.append('g')
        .call(yAxis2);

    //Barras
    svg.selectAll('.barras')
        .data(newDataMuertes)
        .enter()
        .append('rect')
        .attr("x", function(d,i) { return x3(i); })
        .attr("y", function() { return y2(0); })
        .attr("width", x1.bandwidth())
        .attr("height", function(d) { return y2(+d.muertes_media) - y2(0); })
        .attr('fill', '#fd5154')
        .on('touchmove mouseover', function(d,i){
            let fecha = d.fecha.split("/");
            fecha = fecha[0] + "/" + fecha[1];
                        
            tooltipDiv.transition()     
                .duration(200)
                .style('background-color', '#f7f7f7')
                .style('display', 'block')
                .style('opacity','0.9')
            tooltipDiv.html('<span style="font-weight: 300;">Fecha: ' + fecha + '</span><span style="font-weight: 900;">Media muertes: ' + d.muertes_media.replace('.',',') + '</span>')
                .style("left", ""+ (event.pageX - (window.innerWidth < 525 ? 80 : 70)) +"px")     
                .style("top", ""+ (event.pageY - 60) +"px");
        })
        .on('touchend mouseout mouseleave', function(d){
            tooltipDiv.style('display','none').style('opacity','0');
        });
}

/* Helpers para vacunación */
function filteringData(dataFilter, opcion){
    //Filtrados
    let newData = dataFilter.filter((item) => {
        if(item.key == opcion){
            return item;
        }
    })[0];

	return newData;
}

function filteringMaxData(data) {
    let auxData = max(data.values, (d) => { return +d.Nuevas_diarias });
	return auxData;
}

/* Helpers */
let normalize = (function() {
    var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç", 
        to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
        mapping = {};
   
    for(var i = 0, j = from.length; i < j; i++ )
        mapping[ from.charAt( i ) ] = to.charAt( i );
   
    return function( str ) {
        var ret = [];
        for( var i = 0, j = str.length; i < j; i++ ) {
            var c = str.charAt( i );
            if( mapping.hasOwnProperty( str.charAt( i ) ) )
                ret.push( mapping[ c ] );
            else
                ret.push( c );
        }      
        return ret.join( '' );
    }
   
})();

function comparativa(a,b){
    const datoA = normalize(a.values[0].CCAA);
    const datoB = normalize(b.values[0].CCAA);

    let comparacion = 0;
    if (datoA > datoB) {
        comparacion = 1;
    } else if (datoA < datoB) {
        comparacion = -1;
    }
    return comparacion;
}